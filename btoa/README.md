    ./btoa -h
    btoa 0.1.0
    Parse a Blender scene file and export it in Arnold's .ass file format.
    
    USAGE:
        btoa [OPTIONS] <path>
    
    FLAGS:
        -h, --help       Prints help information
        -V, --version    Prints version information
    
    OPTIONS:
        -c, --camera_name <camera-name>            camera name
        -l, --light_scale <light-scale>            global light scaling [default: 1.0]
        -o, --output_filename <output-filename>    output filename (default: "btoa.ass")
    
    ARGS:
        <path>    The path to the file to read

