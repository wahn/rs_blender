// Extract information from .blend file:
// --
// SC
// --
// scale_length
// --
// OB
// --
// base_name
// obmat
// --
// CA
// --
// base_name
// --
// LA
// --
// base_name
// --
// ME
// --
// base_name
// --
// MA
// --
// base_name
// --
// IM
// --

// std
use std::collections::HashMap;
use std::f32::consts::PI;
use std::ffi::OsString;
use std::fs::File;
use std::io::Read;
use std::io::{BufWriter, Write};
use std::mem;
use std::path::Path;
// others
use chrono::prelude::*;
use structopt::StructOpt;

/// Parse a Blender scene file and export it in Arnold's .ass file format.
#[derive(StructOpt)]
struct Cli {
    /// camera name
    #[structopt(short = "c", long = "camera_name")]
    camera_name: Option<String>,
    /// global light scaling
    #[structopt(short = "l", long = "light_scale", default_value = "1.0")]
    light_scale: f32,
    /// output filename (default: "btoa.ass")
    #[structopt(short = "o", long = "output_filename")]
    output_filename: Option<String>,
    /// The path to the file to read
    #[structopt(parse(from_os_str))]
    path: std::path::PathBuf,
}

// BEGIN PBRT

#[derive(Debug, Default, Copy, Clone)]
struct PbrtSphere {
    pub radius: f32,
    // pub zmin: f32,
    // pub zmax: f32,
    // pub phimax: f32,
}

impl PbrtSphere {
    fn new(radius: f32, // , zmin: f32, zmax: f32, phimax: f32
    ) -> Self {
        PbrtSphere {
            radius,
            // zmin,
            // zmax,
            // phimax,
        }
    }
}

// END PBRT

// BEGIN BLENDER

#[derive(Debug, Default, Copy, Clone)]
struct BlendCamera {
    pub lens: f32,
    pub angle_x: f32,
    pub angle_y: f32,
}

#[derive(Debug, Default, Copy, Clone)]
struct Blend279Material {
    pub r: f32,
    pub g: f32,
    pub b: f32,
    pub a: f32,
    pub specr: f32,
    pub specg: f32,
    pub specb: f32,
    pub mirr: f32,
    pub mirg: f32,
    pub mirb: f32,
    pub emit: f32,
    pub ang: f32, // IOR
    pub ray_mirror: f32,
    pub roughness: f32,
}

fn focallength_to_fov(focal_length: f32, sensor: f32) -> f32 {
    2.0_f32 * ((sensor / 2.0_f32) / focal_length).atan()
}

// END BLENDER

// BEGIN ARNOLD

pub enum AssLight {
    AssDistant(AssDistantLight),
    AssPoint(AssPointLight),
}

pub struct AssDistantLight {
    pub color: [f32; 3],
    pub intensity: f32,
}

pub struct AssPointLight {
    pub color: [f32; 3],
    pub intensity: f32,
}

pub struct AssPolyMesh {
    // pub nsides: Vec<u8>,
    pub vidxs: Vec<u32>,
    pub vlist: Vec<[f32; 3]>,
    pub nlist: Vec<[f32; 3]>,
    pub uvlist: Vec<[f32; 2]>,
    pub shidxs: Vec<u8>,
}

pub struct AssSphere {
    pub radius: f32,
}

// END ARNOLD

struct SceneDescription {
    // mesh
    mesh_names: Vec<String>,
    meshes: Vec<AssPolyMesh>,
    mesh_shader_colors: Vec<Vec<[f32; 3]>>,
    // sphere
    sphere_names: Vec<String>,
    spheres: Vec<AssSphere>,
    // lights
    light_names: Vec<String>,
    lights: Vec<AssLight>,
}

struct SceneDescriptionBuilder {
    // mesh
    mesh_names: Vec<String>,
    meshes: Vec<AssPolyMesh>,
    mesh_shader_colors: Vec<Vec<[f32; 3]>>,
    // sphere
    sphere_names: Vec<String>,
    spheres: Vec<AssSphere>,
    // lights
    light_names: Vec<String>,
    lights: Vec<AssLight>,
}

impl SceneDescriptionBuilder {
    fn new() -> SceneDescriptionBuilder {
        SceneDescriptionBuilder {
            mesh_names: Vec::with_capacity(256),
            meshes: Vec::with_capacity(256),
            mesh_shader_colors: Vec::with_capacity(256),
            sphere_names: Vec::new(),
            spheres: Vec::new(),
            light_names: Vec::with_capacity(16),
            lights: Vec::with_capacity(16),
        }
    }
    fn add_mesh(
        &mut self,
        base_name: String,
        vidxs: Vec<u32>,
        vlist: Vec<[f32; 3]>,
        nlist: Vec<[f32; 3]>,
        uvlist: Vec<[f32; 2]>,
        triangle_colors: Vec<[f32; 3]>,
    ) -> &mut SceneDescriptionBuilder {
        self.mesh_names.push(base_name);
        let mut shidxs: Vec<u8> = Vec::with_capacity(triangle_colors.len());
        let mut shader_colors: Vec<[f32; 3]> = Vec::with_capacity(8_usize);
        if triangle_colors.len() > 0_usize {
            for triangle_color_idx in 0..triangle_colors.len() {
                let triangle_color = triangle_colors[triangle_color_idx];
                let mut shidx: u8 = 0;
                let mut found: bool = false;
                if shader_colors.len() > 0_usize {
                    for shader_color_idx in 0..shader_colors.len() {
                        let shader_color = shader_colors[shader_color_idx];
                        if shader_color[0] == triangle_color[0]
                            && shader_color[1] == triangle_color[1]
                            && shader_color[2] == triangle_color[2]
                        {
                            shidx = shader_color_idx as u8;
                            found = true;
                            break;
                        }
                    }
                }
                if !found {
                    shader_colors.push([triangle_color[0], triangle_color[1], triangle_color[2]]);
                    shidx = (shader_colors.len() - 1) as u8;
                }
                let shader_color = shader_colors[shidx as usize];
                // println!("[{}]: {} = {:?}", triangle_color_idx, shidx, shader_color);
                shidxs.push(shidx);
            }
            // println!("{:?}", shader_colors);
        }
        let poly_mesh = AssPolyMesh {
            vidxs,
            vlist,
            nlist,
            uvlist,
            shidxs,
        };
        self.meshes.push(poly_mesh);
        self.mesh_shader_colors.push(shader_colors);
        self
    }
    fn add_sphere(&mut self, base_name: String, radius: f32) -> &mut SceneDescriptionBuilder {
        self.sphere_names.push(base_name);
        let sphere = AssSphere { radius };
        self.spheres.push(sphere);
        self
    }
    fn add_distant_light(
        &mut self,
        base_name: String,
        color: [f32; 3],
        intensity: f32,
    ) -> &mut SceneDescriptionBuilder {
        self.light_names.push(base_name);
        let distant_light = AssDistantLight { color, intensity };
        self.lights.push(AssLight::AssDistant(distant_light));
        self
    }
    fn add_point_light(
        &mut self,
        base_name: String,
        color: [f32; 3],
        intensity: f32,
    ) -> &mut SceneDescriptionBuilder {
        self.light_names.push(base_name);
        let point_light = AssPointLight { color, intensity };
        self.lights.push(AssLight::AssPoint(point_light));
        self
    }
    fn finalize(self) -> SceneDescription {
        SceneDescription {
            mesh_names: self.mesh_names,
            meshes: self.meshes,
            mesh_shader_colors: self.mesh_shader_colors,
            sphere_names: self.sphere_names,
            spheres: self.spheres,
            light_names: self.light_names,
            lights: self.lights,
        }
    }
}

// BEGIN PBRT

/// Convert from angles expressed in radians to degrees.
pub fn degrees(rad: f32) -> f32 {
    (180.0 / PI) * rad
}

// END PBRT

fn decode_blender_header(header: &[u8], version: &mut u32, print_it: bool) -> bool {
    // BLENDER
    match header[0] as char {
        'B' => {
            if print_it {
                print!("B");
            }
        }
        _ => return false,
    }
    match header[1] as char {
        'L' => {
            if print_it {
                print!("L");
            }
        }
        _ => return false,
    }
    match header[2] as char {
        'E' => {
            if print_it {
                print!("E");
            }
        }
        _ => return false,
    }
    match header[3] as char {
        'N' => {
            if print_it {
                print!("N");
            }
        }
        _ => return false,
    }
    match header[4] as char {
        'D' => {
            if print_it {
                print!("D");
            }
        }
        _ => return false,
    }
    match header[5] as char {
        'E' => {
            if print_it {
                print!("E");
            }
        }
        _ => return false,
    }
    match header[6] as char {
        'R' => {
            if print_it {
                print!("R");
            }
        }
        _ => return false,
    }
    // [_|-]
    match header[7] as char {
        '_' => {
            if print_it {
                print!("_");
            }
        }
        '-' => {
            if print_it {
                print!("-");
            }
        }
        _ => return false,
    }
    // [v|V]
    match header[8] as char {
        'v' => {
            if print_it {
                print!("v");
            }
        }
        'V' => {
            if print_it {
                print!("V");
            }
        }
        _ => return false,
    }
    for i in 9..12 {
        if header[i].is_ascii_digit() {
            if print_it {
                print!("{:?}", (header[i] as char).to_digit(10).unwrap());
            }
        } else {
            return false;
        }
    }
    if print_it {
        print!("\n");
    }
    // get the version number
    let last3c = vec![header[9], header[10], header[11]];
    let version_str = String::from_utf8(last3c).unwrap();
    // convert to u32 and return
    *version = version_str.parse::<u32>().unwrap();
    true
}

fn get_material<'s, 'h>(
    mesh_name: &'s String,
    material_hm: &'h HashMap<String, Blend279Material>,
) -> Option<&'h Blend279Material> {
    // first try material with exactly the same name as the mesh
    if let Some(mat) = material_hm.get(mesh_name) {
        return Some(mat);
    } else {
        // then remove trailing digits from mesh name
        let mut ntd: String = String::new();
        let mut chars = mesh_name.chars();
        let mut digits: String = String::new(); // many digits
        while let Some(c) = chars.next() {
            if c.is_digit(10_u32) {
                // collect digits
                digits.push(c);
            } else {
                // push collected digits (if any)
                ntd += &digits;
                // and reset
                digits = String::new();
                // push non-digit
                ntd.push(c);
            }
        }
        // try no trailing digits (ntd)
        if let Some(mat) = material_hm.get(&ntd) {
            return Some(mat);
        } else {
            // finally try adding a '1' at the end
            ntd.push('1');
            if let Some(mat) = material_hm.get(&ntd) {
                return Some(mat);
            } else {
                println!("WARNING: No material found for {:?}", mesh_name);
                return None;
            }
        }
    }
    // material_hm.get(mesh_name)
}

fn make_id(code: &[u8]) -> String {
    let mut id = String::with_capacity(4);
    for i in 0..4 {
        if (code[i] as char).is_ascii_alphanumeric() {
            id.push(code[i] as char);
        }
    }
    id
}

fn read_mesh(
    base_name: &String,
    p: &Vec<[f32; 3]>,
    n: &Vec<[f32; 3]>,
    uvs: &mut Vec<[f32; 2]>,
    loops: &Vec<u8>,
    vertex_indices: Vec<u32>,
    vertex_colors: Vec<u8>,
    is_smooth: bool,
    builder: &mut SceneDescriptionBuilder,
) {
    // println!("p = {:#?}", p);
    // println!("vertex_indices = {:?}", vertex_indices);
    let mut n_ws: Vec<[f32; 3]> = Vec::with_capacity(n.len());
    let mut n_vertices: usize = p.len();
    if is_smooth {
        // println!("n = {:?}", n);
        assert!(n.len() == p.len());
        if !n.is_empty() {
            for i in 0..n_vertices {
                n_ws.push(n[i]);
            }
        }
    }
    let mut uv: Vec<[f32; 2]> = Vec::with_capacity(uvs.len());
    let mut new_vertex_indices: Vec<u32> = Vec::with_capacity(vertex_indices.len());
    let mut p_vi: Vec<[f32; 3]> = Vec::with_capacity(vertex_indices.len());
    if uvs.len() > 0_usize {
        let mut vertex_counter: u32 = 0;
        for vi in &vertex_indices {
            p_vi.push(p[*vi as usize]);
            new_vertex_indices.push(vertex_counter);
            vertex_counter += 1;
        }
        let mut new_uvs: Vec<[f32; 2]> = Vec::with_capacity(uvs.len());
        let mut loop_idx: usize = 0;
        for poly in loops {
            // triangle
            if *poly == 3_u8 {
                for _i in 0..3 {
                    new_uvs.push(uvs[loop_idx]);
                    loop_idx += 1;
                }
            }
            // quad
            else if *poly == 4_u8 {
                new_uvs.push(uvs[loop_idx + 0]);
                new_uvs.push(uvs[loop_idx + 1]);
                new_uvs.push(uvs[loop_idx + 2]);
                new_uvs.push(uvs[loop_idx + 0]);
                new_uvs.push(uvs[loop_idx + 2]);
                new_uvs.push(uvs[loop_idx + 3]);
                loop_idx += 4;
            } else {
                println!("WARNING: quads or triangles expected (poly = {})", poly)
            }
        }
        *uvs = new_uvs;
        assert!(uvs.len() == p_vi.len(), "{} != {}", uvs.len(), p_vi.len());
        n_vertices = p_vi.len();
        for i in 0..n_vertices {
            uv.push(uvs[i]);
        }
    }
    let mut triangle_colors: Vec<[f32; 3]> = Vec::with_capacity(loops.len() * 2);
    if vertex_colors.len() != 0_usize {
        // println!("vertex_colors = {}", vertex_colors.len());
        let mut rgba: usize = 0;
        for l in loops {
            for i in 0..*l {
                let r: f32 = vertex_colors[rgba * 4 + 0] as f32 / 255.0 as f32;
                let g: f32 = vertex_colors[rgba * 4 + 1] as f32 / 255.0 as f32;
                let b: f32 = vertex_colors[rgba * 4 + 2] as f32 / 255.0 as f32;
                // let a: f32 = vertex_colors[rgba*4 + 3] as f32 / 255.0 as f32;
                let s: [f32; 3] = [r, g, b];
                // println!("{}: {:?}", rgba, s);
                if i == 0 {
                    // only store first color in loop (triangle or quad)
                    triangle_colors.push(s);
                    if *l == 4 {
                        // store it twice for quads (two triangles)
                        triangle_colors.push(s);
                    }
                }
                rgba += 1;
            }
        }
    }
    // let world_to_object: Transform = Transform::inverse(&object_to_world);
    if new_vertex_indices.len() != 0 {
        builder.add_mesh(
            base_name.clone(),
            new_vertex_indices.clone(),
            p_vi.to_vec(),
            n_ws.to_vec(),
            uv.to_vec(),
            triangle_colors,
        );
    } else {
        builder.add_mesh(
            base_name.clone(),
            vertex_indices.clone(),
            p.to_vec(),
            n_ws.to_vec(),
            uv.to_vec(),
            triangle_colors,
        );
    }
}

fn read_names(
    f: &mut File,
    nr_names: usize,
    names: &mut Vec<String>,
    byte_counter: &mut usize,
) -> std::io::Result<()> {
    // let mut name_counter: usize = 0;
    let mut buffer = [0; 1];
    loop {
        if names.len() == nr_names {
            break;
        } else {
            let mut name = String::new();
            loop {
                // read only one char/byte
                f.read(&mut buffer)?;
                *byte_counter += 1;
                if buffer[0] == 0 {
                    break;
                } else {
                    name.push(buffer[0] as char);
                }
            }
            // println!("  {:?}", name);
            names.push(name);
            // name_counter += 1;
        }
    }
    // println!("  {} names found in {} bytes", name_counter, byte_counter);
    Ok(())
}

fn read_type_names(
    f: &mut File,
    nr_types: usize,
    type_names: &mut Vec<String>,
    byte_counter: &mut usize,
) -> std::io::Result<()> {
    // let mut name_counter: usize = 0;
    let mut buffer = [0; 1];
    loop {
        if type_names.len() == nr_types {
            break;
        } else {
            let mut name = String::new();
            loop {
                // read only one char/byte
                f.read(&mut buffer)?;
                *byte_counter += 1;
                if buffer[0] == 0 {
                    break;
                } else {
                    name.push(buffer[0] as char);
                }
            }
            // println!("  {:?}", name);
            type_names.push(name);
            // name_counter += 1;
        }
    }
    // println!(
    //     "  {} type names found in {} bytes",
    //     name_counter, byte_counter
    // );
    Ok(())
}

fn main() -> std::io::Result<()> {
    let args = Cli::from_args();
    // Blender
    let mut scale_length: f32 = 1.0;
    let mut resolution_x: u32 = 320;
    let mut resolution_y: u32 = 240;
    let mut resolution_percentage: u16 = 100;
    let mut angle_x: f32;
    let mut angle_y: f32;
    let mut base_name = String::new();
    let mut camera_hm: HashMap<String, BlendCamera> = HashMap::new();
    let mut texture_hm: HashMap<String, OsString> = HashMap::new();
    let mut spheres_hm: HashMap<String, PbrtSphere> = HashMap::new();
    let mut ob_count: usize = 0;
    // let mut object_to_world: [f32; 16] = [
    //     1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0,
    // ];
    let mut p: Vec<[f32; 3]> = Vec::with_capacity(1048576);
    let mut n: Vec<[f32; 3]> = Vec::with_capacity(1048576);
    let mut uvs: Vec<[f32; 2]> = Vec::with_capacity(1048576);
    let mut loops: Vec<u8> = Vec::with_capacity(1048576);
    let mut vertex_indices: Vec<u32> = Vec::with_capacity(1048576);
    let mut vertex_colors: Vec<u8> = Vec::with_capacity(1048576);
    let mut prop_radius: f64 = 1.0;
    let mut hdr_path: OsString = OsString::new();
    // first get the DNA
    let mut names: Vec<String> = Vec::new();
    let mut names_len: usize = 0;
    let mut types: Vec<String> = Vec::new();
    let mut dna_2_type_id: Vec<u16> = Vec::new();
    let mut types_len: usize = 0;
    let mut tlen: Vec<u16> = Vec::new();
    {
        let mut f = File::open(&args.path)?;
        // read exactly 12 bytes
        // let mut counter: usize = 0;
        let mut buffer = [0; 12];
        f.read(&mut buffer)?;
        // counter += 12;
        let mut blender_version: u32 = 0;
        if !decode_blender_header(&buffer, &mut blender_version, true) {
            println!("ERROR: Not a .blend file");
            println!("First 12 bytes:");
            println!("{:?}", buffer);
        } else {
            loop {
                // code
                let mut buffer = [0; 4];
                f.read(&mut buffer)?;
                // counter += 4;
                let code = make_id(&buffer);
                // len
                let mut buffer = [0; 4];
                f.read(&mut buffer)?;
                // counter += 4;
                let mut len: u32 = 0;
                len += (buffer[0] as u32) << 0;
                len += (buffer[1] as u32) << 8;
                len += (buffer[2] as u32) << 16;
                len += (buffer[3] as u32) << 24;
                // for now ignore the old entry
                let mut buffer = [0; 8];
                f.read(&mut buffer)?;
                // counter += 8;
                // get SDNAnr
                let mut buffer = [0; 4];
                f.read(&mut buffer)?;
                // counter += 4;
                // for now ignore the nr entry
                let mut buffer = [0; 4];
                f.read(&mut buffer)?;
                // counter += 4;
                // are we done?
                if code == String::from("ENDB") {
                    break;
                }
                if code == String::from("DNA1") {
                    // println!("{} ({})", code, len);
                    // "SDNANAME" in first 8 bytes
                    let mut buffer = [0; 8];
                    f.read(&mut buffer)?;
                    // counter += 8;
                    let mut sdna_name = String::with_capacity(8);
                    for i in 0..8 {
                        if (buffer[i] as char).is_ascii_alphabetic() {
                            sdna_name.push(buffer[i] as char);
                        }
                    }
                    if sdna_name != String::from("SDNANAME") {
                        // read remaining bytes
                        let mut buffer = vec![0; (len - 8) as usize];
                        f.read(&mut buffer)?;
                    // counter += (len - 8) as usize;
                    } else {
                        let mut buffer = [0; 4];
                        f.read(&mut buffer)?;
                        // counter += 4;
                        let mut nr_names: u32 = 0;
                        nr_names += (buffer[0] as u32) << 0;
                        nr_names += (buffer[1] as u32) << 8;
                        nr_names += (buffer[2] as u32) << 16;
                        nr_names += (buffer[3] as u32) << 24;
                        read_names(&mut f, nr_names as usize, &mut names, &mut names_len)?;
                        // counter += names_len;
                        let mut remaining_bytes: usize = (len - 12) as usize - names_len;
                        // skip pad bytes, read "TYPE" and nr_types
                        let mut buffer = [0; 1];
                        loop {
                            f.read(&mut buffer)?;
                            // counter += 1;
                            if buffer[0] == 0 {
                                // skip pad byte
                                remaining_bytes -= 1;
                            } else if buffer[0] as char == 'T' {
                                remaining_bytes -= 1;
                                break;
                            }
                        }
                        // match 'YPE' ('T' was matched above)
                        let mut buffer = [0; 3];
                        f.read(&mut buffer)?;
                        // counter += 3;
                        remaining_bytes -= 3;
                        if buffer[0] as char == 'Y'
                            && buffer[1] as char == 'P'
                            && buffer[2] as char == 'E'
                        {
                            // nr_types
                            let mut buffer = [0; 4];
                            f.read(&mut buffer)?;
                            // counter += 4;
                            remaining_bytes -= 4;
                            let mut nr_types: u32 = 0;
                            nr_types += (buffer[0] as u32) << 0;
                            nr_types += (buffer[1] as u32) << 8;
                            nr_types += (buffer[2] as u32) << 16;
                            nr_types += (buffer[3] as u32) << 24;
                            read_type_names(&mut f, nr_types as usize, &mut types, &mut types_len)?;
                            // counter += types_len;
                            remaining_bytes -= types_len;
                            // skip pad bytes, read "TLEN"
                            let mut buffer = [0; 1];
                            loop {
                                f.read(&mut buffer)?;
                                // counter += 1;
                                if buffer[0] == 0 {
                                    // skip pad byte
                                    remaining_bytes -= 1;
                                } else if buffer[0] as char == 'T' {
                                    remaining_bytes -= 1;
                                    break;
                                }
                            }
                            // match 'LEN' ('T' was matched above)
                            let mut buffer = [0; 3];
                            f.read(&mut buffer)?;
                            // counter += 3;
                            remaining_bytes -= 3;
                            if buffer[0] as char == 'L'
                                && buffer[1] as char == 'E'
                                && buffer[2] as char == 'N'
                            {
                                // read short (16 bits = 2 bytes) for each type
                                for _i in 0..nr_types as usize {
                                    let mut buffer = [0; 2];
                                    f.read(&mut buffer)?;
                                    // counter += 2;
                                    remaining_bytes -= 2;
                                    let mut type_size: u16 = 0;
                                    type_size += (buffer[0] as u16) << 0;
                                    type_size += (buffer[1] as u16) << 8;
                                    tlen.push(type_size);
                                }
                                // skip pad bytes, read "STRC"
                                let mut buffer = [0; 1];
                                loop {
                                    f.read(&mut buffer)?;
                                    // counter += 1;
                                    if buffer[0] == 0 {
                                        // skip pad byte
                                        remaining_bytes -= 1;
                                    } else if buffer[0] as char == 'S' {
                                        remaining_bytes -= 1;
                                        break;
                                    }
                                }
                                // match 'TRC' ('S' was matched above)
                                let mut buffer = [0; 3];
                                f.read(&mut buffer)?;
                                // counter += 3;
                                remaining_bytes -= 3;
                                if buffer[0] as char == 'T'
                                    && buffer[1] as char == 'R'
                                    && buffer[2] as char == 'C'
                                {
                                    // nr_structs
                                    let mut buffer = [0; 4];
                                    f.read(&mut buffer)?;
                                    // counter += 4;
                                    remaining_bytes -= 4;
                                    let mut nr_structs: u32 = 0;
                                    nr_structs += (buffer[0] as u32) << 0;
                                    nr_structs += (buffer[1] as u32) << 8;
                                    nr_structs += (buffer[2] as u32) << 16;
                                    nr_structs += (buffer[3] as u32) << 24;
                                    for _s in 0..nr_structs as usize {
                                        // read two short values
                                        let mut buffer = [0; 2];
                                        f.read(&mut buffer)?;
                                        // counter += 2;
                                        remaining_bytes -= 2;
                                        let mut type_idx: u16 = 0;
                                        type_idx += (buffer[0] as u16) << 0;
                                        type_idx += (buffer[1] as u16) << 8;
                                        f.read(&mut buffer)?;
                                        // counter += 2;
                                        remaining_bytes -= 2;
                                        let mut short2: u16 = 0;
                                        short2 += (buffer[0] as u16) << 0;
                                        short2 += (buffer[1] as u16) << 8;
                                        dna_2_type_id.push(type_idx);
                                        let tuple_counter: usize = short2 as usize;
                                        for _t in 0..tuple_counter {
                                            // read two short values
                                            let mut buffer = [0; 2];
                                            f.read(&mut buffer)?;
                                            // counter += 2;
                                            remaining_bytes -= 2;
                                            f.read(&mut buffer)?;
                                            // counter += 2;
                                            remaining_bytes -= 2;
                                        }
                                    }
                                } else {
                                    println!("ERROR: \"STRC\" expected, \"S\"{:?} found", buffer)
                                }
                            } else {
                                println!("ERROR: \"TLEN\" expected, \"T\"{:?} found", buffer)
                            }
                        } else {
                            println!("ERROR: \"TYPE\" expected, \"T\"{:?} found", buffer)
                        }
                        // read remaining bytes
                        let mut buffer = vec![0; remaining_bytes];
                        f.read(&mut buffer)?;
                        // counter += remaining_bytes;
                    }
                } else {
                    // read len bytes
                    let mut buffer = vec![0; len as usize];
                    f.read(&mut buffer)?;
                    // counter += len as usize;
                    if code == String::from("OB") {
                        ob_count += 1;
                    }
                }
            }
            // println!("{} bytes read", counter);
        }
    }
    // then use the DNA
    let mut material_hm: HashMap<String, Blend279Material> = HashMap::with_capacity(ob_count);
    let mut object_to_world_hm: HashMap<String, [f32; 16]> = HashMap::with_capacity(ob_count);
    let mut builder: SceneDescriptionBuilder = SceneDescriptionBuilder::new();
    {
        let mut f = File::open(&args.path)?;
        let parent = args.path.parent().unwrap();
        // read exactly 12 bytes
        let mut counter: usize = 0;
        let mut buffer = [0; 12];
        f.read(&mut buffer)?;
        counter += 12;
        let mut blender_version: u32 = 0;
        if !decode_blender_header(&buffer, &mut blender_version, false) {
            println!("ERROR: Not a .blend file");
            println!("First 12 bytes:");
            println!("{:?}", buffer);
        } else {
            let mut data_following_mesh: bool = false;
            let mut data_following_object: bool = false;
            let mut is_smooth: bool = false;
            // Blender
            let mut loop_indices: Vec<u32> = Vec::with_capacity(4194304); // 2^22
            loop {
                // code
                let mut buffer = [0; 4];
                f.read(&mut buffer)?;
                counter += 4;
                let code = make_id(&buffer);
                // len
                let mut buffer = [0; 4];
                f.read(&mut buffer)?;
                counter += 4;
                let mut len: u32 = 0;
                len += (buffer[0] as u32) << 0;
                len += (buffer[1] as u32) << 8;
                len += (buffer[2] as u32) << 16;
                len += (buffer[3] as u32) << 24;
                // if code == String::from("SC")
                //     || code == String::from("OB")
                //     || code == String::from("CA")
                //     || code == String::from("LA")
                //     || code == String::from("ME")
                //     || code == String::from("MA")
                //     || code == String::from("IM")
                // {
                //     println!("{} ({})", code, len);
                // }
                // for now ignore the old entry
                let mut buffer = [0; 8];
                f.read(&mut buffer)?;
                counter += 8;
                // get SDNAnr
                let mut buffer = [0; 4];
                f.read(&mut buffer)?;
                counter += 4;
                let mut sdna_nr: u32 = 0;
                sdna_nr += (buffer[0] as u32) << 0;
                sdna_nr += (buffer[1] as u32) << 8;
                sdna_nr += (buffer[2] as u32) << 16;
                sdna_nr += (buffer[3] as u32) << 24;
                // get data len
                let mut buffer = [0; 4];
                f.read(&mut buffer)?;
                counter += 4;
                let mut data_len: u32 = 0;
                data_len += (buffer[0] as u32) << 0;
                data_len += (buffer[1] as u32) << 8;
                data_len += (buffer[2] as u32) << 16;
                data_len += (buffer[3] as u32) << 24;
                // are we done?
                if code == String::from("ENDB") {
                    break;
                }
                if code == String::from("DNA1") {
                    // read len bytes
                    let mut buffer = vec![0; len as usize];
                    f.read(&mut buffer)?;
                    counter += len as usize;
                    if data_following_object {
                        // TODO: PbrtCylinder, PbrtDisk
                        if base_name.starts_with("PbrtSphere") {
                            // store sphere values for later
                            let pbrt_sphere: PbrtSphere = PbrtSphere::new(prop_radius as f32);
                            spheres_hm.insert(base_name.clone(), pbrt_sphere);
                        }
                    }
                    if data_following_mesh {
                        // TODO: PbrtCylinder, PbrtDisk
                        if base_name.starts_with("PbrtSphere") {
                            // create sphere after mesh data
                            if let Some(sphere) = spheres_hm.get(&base_name) {
                                builder.add_sphere(base_name.clone(), sphere.radius);
                            }
                        } else {
                            read_mesh(
                                &base_name,
                                &p,
                                &n,
                                &mut uvs,
                                &loops,
                                vertex_indices.clone(),
                                vertex_colors.clone(),
                                is_smooth,
                                &mut builder,
                            );
                            vertex_indices.clear();
                            vertex_colors.clear();
                        }
                    }
                    // reset booleans
                    data_following_mesh = false;
                    data_following_object = false;
                    is_smooth = false;
                } else {
                    // read len bytes
                    let mut buffer = vec![0; len as usize];
                    f.read(&mut buffer)?;
                    counter += len as usize;
                    // TODO: OB, ME, SC, CA, MA, LA, DATA
                    if code == String::from("IM") {
                        // IM
                        // println!("{} ({})", code, len);
                        // println!("  SDNAnr = {}", sdna_nr);
                        // v279: Image (len=1992) { ... }
                        // v280: Image (len=1440) { ... }
                        let mut skip_bytes: usize = 0;
                        // id
                        let mut id_name = String::new();
                        base_name = String::new();
                        for i in 32..(32 + 66) {
                            if buffer[i] == 0 {
                                break;
                            }
                            id_name.push(buffer[i] as char);
                            if i != 32 && i != 33 {
                                base_name.push(buffer[i] as char);
                            }
                        }
                        // println!("  id_name = {}", id_name);
                        // println!("  base_name = {}", base_name);
                        if blender_version < 280 {
                            skip_bytes += 120;
                        } else {
                            skip_bytes += 152;
                        }
                        // char name[1024]
                        let mut img_name = String::new();
                        for i in 0..1024 {
                            if buffer[skip_bytes + i] == 0 {
                                break;
                            }
                            img_name.push(buffer[skip_bytes + i] as char);
                        }
                        // skip_bytes += 1024;
                        if img_name.len() > 2 {
                            // println!("  img_name = {}", img_name);
                            let image_path: &Path = Path::new(&img_name);
                            // println!("  image_path = {:?}", image_path);
                            if let Some(img_ext) = image_path.extension() {
                                // println!("  img_ext = {:?}", img_ext);
                                if img_ext == "hdr" {
                                    if image_path.starts_with("//") {
                                        if let Ok(relative) = image_path.strip_prefix("//") {
                                            let canonicalized = parent
                                                .join(relative.clone())
                                                .canonicalize()
                                                .unwrap();
                                            println!("{:?}", canonicalized);
                                            hdr_path = canonicalized.into_os_string();
                                        }
                                    }
                                } else {
                                    if image_path.starts_with("//") {
                                        if let Ok(relative) = image_path.strip_prefix("//") {
                                            let canonicalized = parent
                                                .join(relative.clone())
                                                .canonicalize()
                                                .unwrap();
                                            println!("{:?}", canonicalized);
                                            texture_hm.insert(
                                                base_name.clone(),
                                                canonicalized.into_os_string(),
                                            );
                                        }
                                    }
                                }
                            }
                        }
                    } else if code == String::from("OB") {
                        if data_following_object {
                            // TODO: PbrtCylinder, PbrtDisk
                            if base_name.starts_with("PbrtSphere") {
                                // store sphere values for later
                                let pbrt_sphere: PbrtSphere = PbrtSphere::new(prop_radius as f32);
                                spheres_hm.insert(base_name.clone(), pbrt_sphere);
                            }
                        }
                        // OB
                        // println!("{} ({})", code, len);
                        // println!("  SDNAnr = {}", sdna_nr);
                        // v279: Object (len=1440) { ... }
                        let mut skip_bytes: usize = 0;
                        // id
                        let mut id_name = String::new();
                        base_name = String::new();
                        for i in 32..(32 + 66) {
                            if buffer[i] == 0 {
                                break;
                            }
                            id_name.push(buffer[i] as char);
                            if i != 32 && i != 33 {
                                base_name.push(buffer[i] as char);
                            }
                        }
                        // println!("  id_name = {}", id_name);
                        // println!("  base_name = {}", base_name);
                        if blender_version < 280 {
                            skip_bytes += 120;
                        } else {
                            skip_bytes += 152;
                        }
                        // adt
                        skip_bytes += 8;
                        if blender_version < 280 {
                            // nothing there
                        } else {
                            // DrawDataList (len=16)
                            skip_bytes += 16;
                        }
                        // sculpt
                        skip_bytes += 8;
                        // type
                        // let mut ob_type: u16 = 0;
                        // ob_type += (buffer[skip_bytes] as u16) << 0;
                        // ob_type += (buffer[skip_bytes + 1] as u16) << 8;
                        skip_bytes += 2;
                        // match ob_type {
                        //     0 => println!("  ob_type = {}", "OB_EMPTY"),
                        //     1 => println!("  ob_type = {}", "OB_MESH"),
                        //     11 => println!("  ob_type = {}", "OB_CAMERA"),
                        //     _ => println!("  ob_type = {}", ob_type),
                        // }
                        // partype
                        skip_bytes += 2;
                        // par1, par2, par3
                        skip_bytes += 4 * 3;
                        // parsubstr[64]
                        skip_bytes += 64;
                        // parent, track, proxy, proxy_group, proxy_from
                        skip_bytes += 8 * 5;
                        // ipo
                        skip_bytes += 8;
                        if blender_version < 280 {
                            // bb
                            skip_bytes += 8;
                        } else {
                            // nothing there
                        }
                        // action, poselib, pose, data, gpd
                        skip_bytes += 8 * 5;
                        // v279: bAnimVizSettings (len=48)
                        // v280: bAnimVizSettings (len=32)
                        if blender_version < 280 {
                            skip_bytes += 48;
                        } else {
                            skip_bytes += 32;
                        }
                        // mpath
                        skip_bytes += 8;
                        if blender_version < 280 {
                            // ListBase * 4
                            skip_bytes += 16 * 4;
                        } else {
                            // _pad0
                            skip_bytes += 8;
                            // ListBase * 7
                            skip_bytes += 16 * 7;
                        }
                        // mode, restore_mode
                        skip_bytes += 4 * 2;
                        // mat, matbits
                        skip_bytes += 8 * 2;
                        // totcol, actcol
                        skip_bytes += 4 * 2;
                        // loc
                        skip_bytes += 4 * 3;
                        // dloc
                        skip_bytes += 4 * 3;
                        if blender_version < 280 {
                            // orig
                            skip_bytes += 4 * 3;
                        } else {
                            // nothing there
                        }
                        // size
                        skip_bytes += 4 * 3;
                        // dsize
                        skip_bytes += 4 * 3;
                        // dscale
                        skip_bytes += 4 * 3;
                        // rot
                        for _i in 0..3 {
                            let mut rot_buf: [u8; 4] = [0_u8; 4];
                            for i in 0..4 as usize {
                                rot_buf[i] = buffer[skip_bytes + i];
                            }
                            let _rot: f32 = unsafe { mem::transmute(rot_buf) };
                            // println!("  rot[{}] = {}", i, rot);
                            skip_bytes += 4;
                        }
                        //skip_bytes += 4 * 3;
                        // drot
                        skip_bytes += 4 * 3;
                        // quat
                        skip_bytes += 4 * 4;
                        // dquat
                        skip_bytes += 4 * 4;
                        // rotAxis
                        skip_bytes += 4 * 3;
                        // drotAxis
                        skip_bytes += 4 * 3;
                        // rotAngle
                        let mut rot_angle_buf: [u8; 4] = [0_u8; 4];
                        for i in 0..4 as usize {
                            rot_angle_buf[i] = buffer[skip_bytes + i];
                        }
                        let _rot_angle: f32 = unsafe { mem::transmute(rot_angle_buf) };
                        // println!("  rot_angle = {}", rot_angle);
                        skip_bytes += 4;
                        // drotAngle
                        skip_bytes += 4;
                        // obmat
                        let mut mat_values: [f32; 16] = [0.0_f32; 16];
                        for i in 0..4 {
                            for j in 0..4 {
                                let mut obmat_buf: [u8; 4] = [0_u8; 4];
                                for i in 0..4 as usize {
                                    obmat_buf[i] = buffer[skip_bytes + i];
                                }
                                let obmat: f32 = unsafe { mem::transmute(obmat_buf) };
                                // println!("  obmat[{}][{}] = {}", i, j, obmat);
                                mat_values[i * 4 + j] = obmat;
                                skip_bytes += 4;
                            }
                        }
                        mat_values[12] = mat_values[12] * scale_length;
                        mat_values[13] = mat_values[13] * scale_length;
                        mat_values[14] = mat_values[14] * scale_length;
                        object_to_world_hm.insert(base_name.clone(), mat_values);
                        // parentinv
                        for _i in 0..4 {
                            for _j in 0..4 {
                                let mut parentinv_buf: [u8; 4] = [0_u8; 4];
                                for i in 0..4 as usize {
                                    parentinv_buf[i] = buffer[skip_bytes + i];
                                }
                                let _parentinv: f32 = unsafe { mem::transmute(parentinv_buf) };
                                // println!("  parentinv[{}][{}] = {}", i, j, parentinv);
                                skip_bytes += 4;
                            }
                        }
                        // constinv
                        for _i in 0..4 {
                            for _j in 0..4 {
                                let mut constinv_buf: [u8; 4] = [0_u8; 4];
                                for i in 0..4 as usize {
                                    constinv_buf[i] = buffer[skip_bytes + i];
                                }
                                let _constinv: f32 = unsafe { mem::transmute(constinv_buf) };
                                // println!("  constinv[{}][{}] = {}", i, j, constinv);
                                skip_bytes += 4;
                            }
                        }
                        // imat
                        for _i in 0..4 {
                            for _j in 0..4 {
                                let mut imat_buf: [u8; 4] = [0_u8; 4];
                                for i in 0..4 as usize {
                                    imat_buf[i] = buffer[skip_bytes + i];
                                }
                                let _imat: f32 = unsafe { mem::transmute(imat_buf) };
                                // println!("  imat[{}][{}] = {}", i, j, imat);
                                skip_bytes += 4;
                            }
                        }
                        // imat_ren
                        for _i in 0..4 {
                            for _j in 0..4 {
                                let mut imat_ren_buf: [u8; 4] = [0_u8; 4];
                                for i in 0..4 as usize {
                                    imat_ren_buf[i] = buffer[skip_bytes + i];
                                }
                                let _imat_ren: f32 = unsafe { mem::transmute(imat_ren_buf) };
                                // println!("  imat_ren[{}][{}] = {}", i, j, imat_ren);
                                skip_bytes += 4;
                            }
                        }
                        // reset booleans
                        data_following_mesh = false;
                        data_following_object = true;
                        is_smooth = false;
                    } else if code == String::from("ME") {
                        if data_following_object {
                            // TODO: PbrtCylinder, PbrtDisk
                            if base_name.starts_with("PbrtSphere") {
                                // store sphere values for later
                                let pbrt_sphere: PbrtSphere = PbrtSphere::new(prop_radius as f32);
                                spheres_hm.insert(base_name.clone(), pbrt_sphere);
                            }
                        }
                        if data_following_mesh {
                            // TODO: PbrtCylinder, PbrtDisk
                            if base_name.starts_with("PbrtSphere") {
                                // create sphere after mesh data
                                if let Some(sphere) = spheres_hm.get(&base_name) {
                                    builder.add_sphere(base_name.clone(), sphere.radius);
                                }
                            } else {
                                read_mesh(
                                    &base_name,
                                    &p,
                                    &n,
                                    &mut uvs,
                                    &loops,
                                    vertex_indices.clone(),
                                    vertex_colors.clone(),
                                    is_smooth,
                                    &mut builder,
                                );
                                vertex_indices.clear();
                                vertex_colors.clear();
                            }
                        }
                        // ME
                        // println!("{} ({})", code, len);
                        // println!("  SDNAnr = {}", sdna_nr);
                        // Mesh (len=1416) { ... }
                        // let mut skip_bytes: usize = 0;
                        // id
                        let mut id_name = String::new();
                        base_name = String::new();
                        for i in 32..(32 + 66) {
                            if buffer[i] == 0 {
                                break;
                            }
                            id_name.push(buffer[i] as char);
                            if i != 32 && i != 33 {
                                base_name.push(buffer[i] as char);
                            }
                        }
                        // println!("  id_name = {}", id_name);
                        // println!("  base_name = {}", base_name);
                        // data_following_mesh
                        data_following_mesh = true;
                        // clear all Vecs
                        p.clear();
                        n.clear();
                        uvs.clear();
                        loops.clear();
                        vertex_indices.clear();
                        vertex_colors.clear();
                        loop_indices.clear();
                    } else if code == String::from("SC") {
                        // SC
                        // println!("{} ({})", code, len);
                        // println!("  SDNAnr = {}", sdna_nr);
                        let mut skip_bytes: usize = 0;
                        let mut id_name = String::new();
                        base_name = String::new();
                        for i in 32..(32 + 66) {
                            if buffer[i] == 0 {
                                break;
                            }
                            id_name.push(buffer[i] as char);
                            if i != 32 && i != 33 {
                                base_name.push(buffer[i] as char);
                            }
                        }
                        // println!("  id_name = {}", id_name);
                        // println!("  base_name = {}", base_name);
                        if blender_version < 280 {
                            skip_bytes += 120;
                        } else {
                            skip_bytes += 152;
                        }
                        // adt
                        skip_bytes += 8;
                        // camera, world, set
                        skip_bytes += 8 * 3;
                        // ListBase * 1
                        skip_bytes += 16 * 1;
                        // basact
                        skip_bytes += 8;
                        if blender_version < 280 {
                            // obedit
                            skip_bytes += 8;
                            // cursor
                            skip_bytes += 4 * 3;
                            // twcent
                            skip_bytes += 4 * 3;
                            // twmin
                            skip_bytes += 4 * 3;
                            // twmax
                            skip_bytes += 4 * 3;
                        } else {
                            // _pad1
                            skip_bytes += 8;
                            // View3DCursor (len=64)
                            skip_bytes += 64;
                        }
                        // lay, layact, lay_updated/_pad2[4]
                        skip_bytes += 4 * 3;
                        // flag
                        skip_bytes += 2;
                        // use_nodes
                        skip_bytes += 1;
                        // pad/_pad3
                        skip_bytes += 1;
                        // nodetree, ed, toolsettings, stats/_pad4
                        skip_bytes += 8 * 4;
                        // DisplaySafeAreas (len=32)
                        skip_bytes += 32;
                        // v279: RenderData (len=4432)
                        // v280: RenderData (len=4192)
                        let mut render_data_bytes: usize = 0;
                        // ImageFormatData (len=256)
                        skip_bytes += 256;
                        render_data_bytes += 256;
                        // avicodecdata
                        skip_bytes += 8;
                        render_data_bytes += 8;
                        if blender_version < 280 {
                            // qtcodecdata
                            skip_bytes += 8;
                            render_data_bytes += 8;
                            // QuicktimeCodecSettings (len=64)
                            skip_bytes += 64;
                            render_data_bytes += 64;
                        } else {
                            // nothing there
                        }
                        // FFMpegCodecData (len=88)
                        skip_bytes += 88;
                        render_data_bytes += 88;
                        // cfra
                        // let mut cfra: u32 = 0;
                        // cfra += (buffer[skip_bytes] as u32) << 0;
                        // cfra += (buffer[skip_bytes + 1] as u32) << 8;
                        // cfra += (buffer[skip_bytes + 2] as u32) << 16;
                        // cfra += (buffer[skip_bytes + 3] as u32) << 24;
                        // println!("    cfra = {}", cfra);
                        skip_bytes += 4;
                        render_data_bytes += 4;
                        // sfra
                        // let mut sfra: u32 = 0;
                        // sfra += (buffer[skip_bytes] as u32) << 0;
                        // sfra += (buffer[skip_bytes + 1] as u32) << 8;
                        // sfra += (buffer[skip_bytes + 2] as u32) << 16;
                        // sfra += (buffer[skip_bytes + 3] as u32) << 24;
                        // println!("    sfra = {}", sfra);
                        skip_bytes += 4;
                        render_data_bytes += 4;
                        // efra
                        // let mut efra: u32 = 0;
                        // efra += (buffer[skip_bytes] as u32) << 0;
                        // efra += (buffer[skip_bytes + 1] as u32) << 8;
                        // efra += (buffer[skip_bytes + 2] as u32) << 16;
                        // efra += (buffer[skip_bytes + 3] as u32) << 24;
                        // println!("    efra = {}", efra);
                        skip_bytes += 4;
                        render_data_bytes += 4;
                        // subframe
                        skip_bytes += 4;
                        render_data_bytes += 4;
                        // psfra, pefra, images, framapto
                        skip_bytes += 4 * 4;
                        render_data_bytes += 4 * 4;
                        // flag, threads
                        skip_bytes += 2 * 2;
                        render_data_bytes += 2 * 2;
                        // framelen, blurfac,
                        skip_bytes += 4 * 2;
                        render_data_bytes += 4 * 2;
                        if blender_version < 280 {
                            // edgeR, edgeG, edgeB
                            skip_bytes += 4 * 3;
                            render_data_bytes += 4 * 3;
                            // fullscreen, xplay, yplay, freqplay, depth, attrib
                            skip_bytes += 2 * 6;
                            render_data_bytes += 2 * 6;
                        } else {
                            // nothing there
                        }
                        // frame_step
                        skip_bytes += 4;
                        render_data_bytes += 4;
                        // stereomode, dimensionspreset
                        skip_bytes += 2 * 2;
                        render_data_bytes += 2 * 2;
                        if blender_version < 280 {
                            // filtertype
                            skip_bytes += 2;
                            render_data_bytes += 2;
                        } else {
                            // nothing there
                        }
                        // size in %
                        resolution_percentage = 0;
                        resolution_percentage += (buffer[skip_bytes] as u16) << 0;
                        resolution_percentage += (buffer[skip_bytes + 1] as u16) << 8;
                        // println!("resolution_percentage: {}", resolution_percentage);
                        skip_bytes += 2;
                        render_data_bytes += 2;
                        if blender_version < 280 {
                            // maximsize, pad6
                            skip_bytes += 2;
                            render_data_bytes += 2;
                        } else {
                            // nothing there
                        }
                        // pad6
                        skip_bytes += 2;
                        render_data_bytes += 2;
                        // xsch
                        let mut xsch: u32 = 0;
                        xsch += (buffer[skip_bytes] as u32) << 0;
                        xsch += (buffer[skip_bytes + 1] as u32) << 8;
                        xsch += (buffer[skip_bytes + 2] as u32) << 16;
                        xsch += (buffer[skip_bytes + 3] as u32) << 24;
                        // println!("    xsch = {}", xsch);
                        skip_bytes += 4;
                        render_data_bytes += 4;
                        resolution_x = xsch;
                        // ysch
                        let mut ysch: u32 = 0;
                        ysch += (buffer[skip_bytes] as u32) << 0;
                        ysch += (buffer[skip_bytes + 1] as u32) << 8;
                        ysch += (buffer[skip_bytes + 2] as u32) << 16;
                        ysch += (buffer[skip_bytes + 3] as u32) << 24;
                        // println!("    ysch = {}", ysch);
                        skip_bytes += 4;
                        render_data_bytes += 4;
                        resolution_y = ysch;
                        // skip remaining RenderData
                        if blender_version < 280 {
                            skip_bytes += 4432 - render_data_bytes;
                        } else {
                            skip_bytes += 4192 - render_data_bytes;
                        }
                        // AudioData (len=32)
                        skip_bytes += 32;
                        // ListBase * 2
                        skip_bytes += 16 * 2;
                        if blender_version < 280 {
                            // nothing there
                        } else {
                            // TransformOrientationSlot (16) * 4
                            skip_bytes += 16 * 4;
                        }
                        // sound_scene, playback_handle, sound_scrub_handle, speaker_handles, fps_info
                        skip_bytes += 8 * 5;
                        // depsgraph/depsgraph_hash
                        skip_bytes += 8;
                        if blender_version < 280 {
                            // pad1, theDag
                            skip_bytes += 8 * 2;
                            // dagflags, pad3
                            skip_bytes += 2 * 2;
                        } else {
                            // _pad7
                            skip_bytes += 4;
                        }
                        // active_keyingset
                        skip_bytes += 4;
                        // ListBase * 1
                        skip_bytes += 16 * 1;
                        if blender_version < 280 {
                            // GameFraming (len=16)
                            skip_bytes += 16;
                            // GameData (len=192)
                            skip_bytes += 192;
                        } else {
                            // nothing there
                        }
                        // v279: UnitSettings (len=8)
                        // v280: UnitSettings (len=16)
                        // scale_length
                        let mut scale_length_buf: [u8; 4] = [0_u8; 4];
                        for i in 0..4 as usize {
                            scale_length_buf[i] = buffer[skip_bytes + i];
                        }
                        scale_length = unsafe { mem::transmute(scale_length_buf) };
                        // println!("    scale_length = {}", scale_length);
                        // skip_bytes += 4;
                        // reset booleans
                        data_following_mesh = false;
                        data_following_object = false;
                        is_smooth = false;
                    } else if code == String::from("CA") {
                        // CA
                        // println!("{} ({})", code, len);
                        // println!("  SDNAnr = {}", sdna_nr);
                        // v279: Camera (len=248) { ... }
                        // v280: Camera (len=520) { ... }
                        let mut skip_bytes: usize = 0;
                        // v279: ID (len=120)
                        // v280: ID (len=152)
                        let mut id_name = String::new();
                        base_name = String::new();
                        for i in 32..(32 + 66) {
                            if buffer[i] == 0 {
                                break;
                            }
                            id_name.push(buffer[i] as char);
                            if i != 32 && i != 33 {
                                base_name.push(buffer[i] as char);
                            }
                        }
                        // println!("  id_name = {}", id_name);
                        // println!("  base_name = {}", base_name);
                        if blender_version < 280 {
                            skip_bytes += 120;
                        } else {
                            skip_bytes += 152;
                        }
                        // adt
                        skip_bytes += 8;
                        // type, dtx
                        skip_bytes += 2;
                        // flag
                        skip_bytes += 2;
                        // passepartalpha
                        skip_bytes += 4;
                        // clipsta
                        // let mut clipsta_buf: [u8; 4] = [0_u8; 4];
                        // for i in 0..4 as usize {
                        //     clipsta_buf[i] = buffer[skip_bytes + i];
                        // }
                        // let clipsta: f32 = unsafe { mem::transmute(clipsta_buf) };
                        // println!("  clipsta = {}", clipsta);
                        skip_bytes += 4;
                        // clipend
                        // let mut clipend_buf: [u8; 4] = [0_u8; 4];
                        // for i in 0..4 as usize {
                        //     clipend_buf[i] = buffer[skip_bytes + i];
                        // }
                        // let clipend: f32 = unsafe { mem::transmute(clipend_buf) };
                        // println!("  clipend = {}", clipend);
                        skip_bytes += 4;
                        // lens
                        let mut lens_buf: [u8; 4] = [0_u8; 4];
                        for i in 0..4 as usize {
                            lens_buf[i] = buffer[skip_bytes + i];
                        }
                        let lens: f32 = unsafe { mem::transmute(lens_buf) };
                        // println!("  lens = {}", lens);
                        skip_bytes += 4;
                        // ortho_scale
                        // let mut ortho_scale_buf: [u8; 4] = [0_u8; 4];
                        // for i in 0..4 as usize {
                        //     ortho_scale_buf[i] = buffer[skip_bytes + i];
                        // }
                        // let ortho_scale: f32 = unsafe { mem::transmute(ortho_scale_buf) };
                        // println!("  ortho_scale = {}", ortho_scale);
                        skip_bytes += 4;
                        // drawsize
                        // let mut drawsize_buf: [u8; 4] = [0_u8; 4];
                        // for i in 0..4 as usize {
                        //     drawsize_buf[i] = buffer[skip_bytes + i];
                        // }
                        // let drawsize: f32 = unsafe { mem::transmute(drawsize_buf) };
                        // println!("  drawsize = {}", drawsize);
                        skip_bytes += 4;
                        // sensor_x
                        let mut sensor_x_buf: [u8; 4] = [0_u8; 4];
                        for i in 0..4 as usize {
                            sensor_x_buf[i] = buffer[skip_bytes + i];
                        }
                        let sensor_x: f32 = unsafe { mem::transmute(sensor_x_buf) };
                        // println!("  sensor_x = {}", sensor_x);
                        skip_bytes += 4;
                        // sensor_y
                        let mut sensor_y_buf: [u8; 4] = [0_u8; 4];
                        for i in 0..4 as usize {
                            sensor_y_buf[i] = buffer[skip_bytes + i];
                        }
                        let sensor_y: f32 = unsafe { mem::transmute(sensor_y_buf) };
                        // println!("  sensor_y = {}", sensor_y);
                        // skip_bytes += 4;
                        // calculate angle_x and angle_y
                        angle_x = degrees(focallength_to_fov(lens, sensor_x) as f32);
                        angle_y = degrees(focallength_to_fov(lens, sensor_y) as f32);
                        let cam: BlendCamera = BlendCamera {
                            lens: lens,
                            angle_x: angle_x,
                            angle_y: angle_y,
                        };
                        camera_hm.insert(base_name.clone(), cam);
                        // reset booleans
                        data_following_mesh = false;
                        data_following_object = false;
                        is_smooth = false;
                    } else if code == String::from("MA") {
                        if data_following_object {
                            // TODO: PbrtCylinder, PbrtDisk
                            if base_name.starts_with("PbrtSphere") {
                                // store sphere values for later
                                let pbrt_sphere: PbrtSphere = PbrtSphere::new(prop_radius as f32);
                                spheres_hm.insert(base_name.clone(), pbrt_sphere);
                            }
                        }
                        if data_following_mesh {
                            // TODO: PbrtCylinder, PbrtDisk
                            if base_name.starts_with("PbrtSphere") {
                                // create sphere after mesh data
                                if let Some(sphere) = spheres_hm.get(&base_name) {
                                    builder.add_sphere(base_name.clone(), sphere.radius);
                                }
                            } else {
                                read_mesh(
                                    &base_name,
                                    &p,
                                    &n,
                                    &mut uvs,
                                    &loops,
                                    vertex_indices.clone(),
                                    vertex_colors.clone(),
                                    is_smooth,
                                    &mut builder,
                                );
                                vertex_indices.clear();
                                vertex_colors.clear();
                            }
                        }
                        // TODO: MA
                        // println!("{} ({})", code, len);
                        // println!("  SDNAnr = {}", sdna_nr);
                        // v279: Material (len=1528) { ... }
                        // v280: Material (len=320) { ... }
                        let mut skip_bytes: usize = 0;
                        // v279: ID (len=120)
                        // v280: ID (len=152)
                        let mut id_name = String::new();
                        base_name = String::new();
                        for i in 32..(32 + 66) {
                            if buffer[i] == 0 {
                                break;
                            }
                            id_name.push(buffer[i] as char);
                            if i != 32 && i != 33 {
                                base_name.push(buffer[i] as char);
                            }
                        }
                        // println!("  id_name = {}", id_name);
                        // println!("  base_name = {}", base_name);
                        if blender_version < 280 {
                            skip_bytes += 120;
                        } else {
                            skip_bytes += 152;
                        }
                        // adt
                        skip_bytes += 8;
                        if blender_version < 280 {
                            // material_type, flag
                            skip_bytes += 2 * 2;
                            // r
                            let mut r_buf: [u8; 4] = [0_u8; 4];
                            for i in 0..4 as usize {
                                r_buf[i] = buffer[skip_bytes + i];
                            }
                            let r: f32 = unsafe { mem::transmute(r_buf) };
                            // println!("  r = {}", r);
                            skip_bytes += 4;
                            // g
                            let mut g_buf: [u8; 4] = [0_u8; 4];
                            for i in 0..4 as usize {
                                g_buf[i] = buffer[skip_bytes + i];
                            }
                            let g: f32 = unsafe { mem::transmute(g_buf) };
                            // println!("  g = {}", g);
                            skip_bytes += 4;
                            // b
                            let mut b_buf: [u8; 4] = [0_u8; 4];
                            for i in 0..4 as usize {
                                b_buf[i] = buffer[skip_bytes + i];
                            }
                            let b: f32 = unsafe { mem::transmute(b_buf) };
                            // println!("  b = {}", b);
                            skip_bytes += 4;
                            // specr
                            let mut specr_buf: [u8; 4] = [0_u8; 4];
                            for i in 0..4 as usize {
                                specr_buf[i] = buffer[skip_bytes + i];
                            }
                            let specr: f32 = unsafe { mem::transmute(specr_buf) };
                            // println!("  specr = {}", specr);
                            skip_bytes += 4;
                            // specg
                            let mut specg_buf: [u8; 4] = [0_u8; 4];
                            for i in 0..4 as usize {
                                specg_buf[i] = buffer[skip_bytes + i];
                            }
                            let specg: f32 = unsafe { mem::transmute(specg_buf) };
                            // println!("  specg = {}", specg);
                            skip_bytes += 4;
                            // specb
                            let mut specb_buf: [u8; 4] = [0_u8; 4];
                            for i in 0..4 as usize {
                                specb_buf[i] = buffer[skip_bytes + i];
                            }
                            let specb: f32 = unsafe { mem::transmute(specb_buf) };
                            // println!("  specb = {}", specb);
                            skip_bytes += 4;
                            // mirr
                            let mut mirr_buf: [u8; 4] = [0_u8; 4];
                            for i in 0..4 as usize {
                                mirr_buf[i] = buffer[skip_bytes + i];
                            }
                            let mirr: f32 = unsafe { mem::transmute(mirr_buf) };
                            // println!("  mirr = {}", mirr);
                            skip_bytes += 4;
                            // mirg
                            let mut mirg_buf: [u8; 4] = [0_u8; 4];
                            for i in 0..4 as usize {
                                mirg_buf[i] = buffer[skip_bytes + i];
                            }
                            let mirg: f32 = unsafe { mem::transmute(mirg_buf) };
                            // println!("  mirg = {}", mirg);
                            skip_bytes += 4;
                            // mirb
                            let mut mirb_buf: [u8; 4] = [0_u8; 4];
                            for i in 0..4 as usize {
                                mirb_buf[i] = buffer[skip_bytes + i];
                            }
                            let mirb: f32 = unsafe { mem::transmute(mirb_buf) };
                            // println!("  mirb = {}", mirb);
                            skip_bytes += 4;
                            // ambr, ambg, ambb
                            skip_bytes += 4 * 3;
                            // amb
                            skip_bytes += 4;
                            // emit
                            let mut emit_buf: [u8; 4] = [0_u8; 4];
                            for i in 0..4 as usize {
                                emit_buf[i] = buffer[skip_bytes + i];
                            }
                            let emit: f32 = unsafe { mem::transmute(emit_buf) };
                            // println!("  emit = {}", emit);
                            skip_bytes += 4;
                            // ang (called "IOR" in Blender's UI)
                            let mut ang_buf: [u8; 4] = [0_u8; 4];
                            for i in 0..4 as usize {
                                ang_buf[i] = buffer[skip_bytes + i];
                            }
                            let ang: f32 = unsafe { mem::transmute(ang_buf) };
                            // println!("  ang = {}", ang);
                            skip_bytes += 4;
                            // spectra
                            skip_bytes += 4;
                            // ray_mirror
                            let mut ray_mirror_buf: [u8; 4] = [0_u8; 4];
                            for i in 0..4 as usize {
                                ray_mirror_buf[i] = buffer[skip_bytes + i];
                            }
                            let ray_mirror: f32 = unsafe { mem::transmute(ray_mirror_buf) };
                            // println!("  ray_mirror = {}", ray_mirror);
                            skip_bytes += 4;
                            // alpha, ref, spec, zoffs, add, translucency
                            skip_bytes += 4 * 6;
                            // VolumeSettings (len=88)
                            skip_bytes += 88;
                            // GameSettings (len=16)
                            skip_bytes += 16;
                            // 7 floats
                            skip_bytes += 4 * 7;
                            // 3 shorts
                            skip_bytes += 2 * 3;
                            // 2 chars
                            skip_bytes += 2;
                            // 2 floats
                            skip_bytes += 4 * 2;
                            // 2 shorts
                            skip_bytes += 2 * 2;
                            // 4 floats
                            skip_bytes += 4 * 4;
                            // 2 shorts
                            skip_bytes += 2 * 2;
                            // 4 ints
                            skip_bytes += 4 * 4;
                            // 4 shorts
                            skip_bytes += 2 * 4;
                            // 10 floats
                            skip_bytes += 4 * 10;
                            // 64 chars
                            skip_bytes += 64;
                            // 3 floats
                            skip_bytes += 4 * 3;
                            // 1 int
                            skip_bytes += 4;
                            // 4 chars
                            skip_bytes += 4;
                            // 3 shorts
                            skip_bytes += 2 * 3;
                            // 2 chars
                            skip_bytes += 2;
                            // 2 shorts
                            skip_bytes += 2 * 2;
                            // roughness
                            let mut roughness_buf: [u8; 4] = [0_u8; 4];
                            for i in 0..4 as usize {
                                roughness_buf[i] = buffer[skip_bytes + i];
                            }
                            let roughness: f32 = unsafe { mem::transmute(roughness_buf) };
                            // println!("  roughness = {}", roughness);
                            // skip_bytes += 4;
                            // Blend279Material
                            let mat: Blend279Material = Blend279Material {
                                r: r,
                                g: g,
                                b: b,
                                a: 1.0,
                                specr: specr,
                                specg: specg,
                                specb: specb,
                                mirr: mirr,
                                mirg: mirg,
                                mirb: mirb,
                                emit: emit,
                                ang: ang,
                                ray_mirror: ray_mirror,
                                roughness: roughness,
                            };
                            // println!("  mat[{:?}] = {:?}", base_name, mat);
                            material_hm.insert(base_name.clone(), mat);
                        } else {
                            // flag
                            skip_bytes += 2;
                            // _pad1
                            skip_bytes += 2;
                            // r
                            let mut r_buf: [u8; 4] = [0_u8; 4];
                            for i in 0..4 as usize {
                                r_buf[i] = buffer[skip_bytes + i];
                            }
                            let r: f32 = unsafe { mem::transmute(r_buf) };
                            // println!("  r = {}", r);
                            skip_bytes += 4;
                            // g
                            let mut g_buf: [u8; 4] = [0_u8; 4];
                            for i in 0..4 as usize {
                                g_buf[i] = buffer[skip_bytes + i];
                            }
                            let g: f32 = unsafe { mem::transmute(g_buf) };
                            // println!("  g = {}", g);
                            skip_bytes += 4;
                            // b
                            let mut b_buf: [u8; 4] = [0_u8; 4];
                            for i in 0..4 as usize {
                                b_buf[i] = buffer[skip_bytes + i];
                            }
                            let b: f32 = unsafe { mem::transmute(b_buf) };
                            // println!("  b = {}", b);
                            skip_bytes += 4;
                            // a
                            let mut a_buf: [u8; 4] = [0_u8; 4];
                            for i in 0..4 as usize {
                                a_buf[i] = buffer[skip_bytes + i];
                            }
                            let a: f32 = unsafe { mem::transmute(a_buf) };
                            // println!("  a = {}", a);
                            skip_bytes += 4;
                            // specr
                            let mut specr_buf: [u8; 4] = [0_u8; 4];
                            for i in 0..4 as usize {
                                specr_buf[i] = buffer[skip_bytes + i];
                            }
                            let specr: f32 = unsafe { mem::transmute(specr_buf) };
                            // println!("  specr = {}", specr);
                            skip_bytes += 4;
                            // specg
                            let mut specg_buf: [u8; 4] = [0_u8; 4];
                            for i in 0..4 as usize {
                                specg_buf[i] = buffer[skip_bytes + i];
                            }
                            let specg: f32 = unsafe { mem::transmute(specg_buf) };
                            // println!("  specg = {}", specg);
                            skip_bytes += 4;
                            // specb
                            let mut specb_buf: [u8; 4] = [0_u8; 4];
                            for i in 0..4 as usize {
                                specb_buf[i] = buffer[skip_bytes + i];
                            }
                            let specb: f32 = unsafe { mem::transmute(specb_buf) };
                            // println!("  specb = {}", specb);
                            skip_bytes += 4;
                            // alpha
                            skip_bytes += 4;
                            // ray_mirror
                            let mut ray_mirror_buf: [u8; 4] = [0_u8; 4];
                            for i in 0..4 as usize {
                                ray_mirror_buf[i] = buffer[skip_bytes + i];
                            }
                            let ray_mirror: f32 = unsafe { mem::transmute(ray_mirror_buf) };
                            // println!("  ray_mirror = {}", ray_mirror);
                            skip_bytes += 4;
                            // spec, gloss_mir, roughness, metallic
                            skip_bytes += 4 * 4;
                            // use_nodes
                            let _use_nodes: u8 = buffer[skip_bytes] as u8;
                            // println!("  use_nodes = {}", use_nodes);
                            // skip_bytes += 1;
                            let mat: Blend279Material = Blend279Material {
                                r: r,
                                g: g,
                                b: b,
                                a: a,
                                specr: specr,
                                specg: specg,
                                specb: specb,
                                mirr: 0.0,
                                mirg: 0.0,
                                mirb: 0.0,
                                emit: 0.0,
                                ang: 1.0,
                                ray_mirror: ray_mirror,
                                roughness: 0.0,
                            };
                            // println!("  mat[{:?}] = {:?}", base_name, mat);
                            material_hm.insert(base_name.clone(), mat);
                        }
                        // reset booleans
                        data_following_mesh = false;
                        data_following_object = false;
                        is_smooth = false;
                    } else if code == String::from("LA") {
                        // LA
                        // println!("{} ({})", code, len);
                        // println!("  SDNAnr = {}", sdna_nr);
                        // v279: Lamp (len=536) { ... }
                        // v280: Lamp (len=TODO) { ... }
                        let mut skip_bytes: usize = 0;
                        // id
                        let mut id_name = String::new();
                        base_name = String::new();
                        for i in 32..(32 + 66) {
                            if buffer[i] == 0 {
                                break;
                            }
                            id_name.push(buffer[i] as char);
                            if i != 32 && i != 33 {
                                base_name.push(buffer[i] as char);
                            }
                        }
                        // println!("  id_name = {}", id_name);
                        // println!("  base_name = {}", base_name);
                        if blender_version < 280 {
                            skip_bytes += 120;
                        } else {
                            skip_bytes += 152;
                        }
                        // adt
                        skip_bytes += 8;
                        // type
                        let mut la_type: u16 = 0;
                        la_type += (buffer[skip_bytes] as u16) << 0;
                        la_type += (buffer[skip_bytes + 1] as u16) << 8;
                        // println!("  la_type = {}", la_type);
                        skip_bytes += 2;
                        // flag
                        skip_bytes += 2;
                        // mode
                        skip_bytes += 4;
                        // colormodel, totex
                        skip_bytes += 2 * 2;
                        // r
                        let mut r_buf: [u8; 4] = [0_u8; 4];
                        for i in 0..4 as usize {
                            r_buf[i] = buffer[skip_bytes + i];
                        }
                        let r: f32 = unsafe { mem::transmute(r_buf) };
                        // println!("  r = {}", r);
                        skip_bytes += 4;
                        // g
                        let mut g_buf: [u8; 4] = [0_u8; 4];
                        for i in 0..4 as usize {
                            g_buf[i] = buffer[skip_bytes + i];
                        }
                        let g: f32 = unsafe { mem::transmute(g_buf) };
                        // println!("  g = {}", g);
                        skip_bytes += 4;
                        // b
                        let mut b_buf: [u8; 4] = [0_u8; 4];
                        for i in 0..4 as usize {
                            b_buf[i] = buffer[skip_bytes + i];
                        }
                        let b: f32 = unsafe { mem::transmute(b_buf) };
                        // println!("  b = {}", b);
                        skip_bytes += 4;
                        // k, shdwr, shdwg, shdwb, shdwpad
                        skip_bytes += 4 * 5;
                        // energy
                        let mut energy_buf: [u8; 4] = [0_u8; 4];
                        for i in 0..4 as usize {
                            energy_buf[i] = buffer[skip_bytes + i];
                        }
                        let energy: f32 = unsafe { mem::transmute(energy_buf) };
                        // println!("  energy = {}", energy);
                        // skip_bytes += 4;
                        // check light type
                        if la_type == 0 {
                            // LA_LOCAL
                            let l: [f32; 3] = [r, g, b];
                            // point light
                            builder.add_point_light(
                                base_name.clone(),
                                l,
                                args.light_scale * energy,
                            );
                        } else if la_type == 1 {
                            // LA_SUN
                            let l: [f32; 3] = [r, g, b];
                            // distant light
                            builder.add_distant_light(
                                base_name.clone(),
                                l,
                                args.light_scale * energy,
                            );
                        } else {
                            println!("WARNING: la_type = {} not supported (yet)", la_type);
                        }
                    } else if code == String::from("DATA") {
                        // DATA
                        if data_following_mesh {
                            // type_id
                            let type_id: usize = dna_2_type_id[sdna_nr as usize] as usize;
                            if types[type_id] == "MPoly" {
                                // println!("{}[{}] ({})", code, data_len, len);
                                // println!("  SDNAnr = {}", sdna_nr);
                                // println!("  {} ({})", types[type_id], tlen[type_id]);
                                let mut skip_bytes: usize = 0;
                                for _p in 0..data_len {
                                    // println!("  {}:", p + 1);
                                    // loopstart
                                    let mut loopstart: u32 = 0;
                                    loopstart += (buffer[skip_bytes] as u32) << 0;
                                    loopstart += (buffer[skip_bytes + 1] as u32) << 8;
                                    loopstart += (buffer[skip_bytes + 2] as u32) << 16;
                                    loopstart += (buffer[skip_bytes + 3] as u32) << 24;
                                    // println!("    loopstart = {}", loopstart);
                                    skip_bytes += 4;
                                    // totloop
                                    let mut totloop: u32 = 0;
                                    totloop += (buffer[skip_bytes] as u32) << 0;
                                    totloop += (buffer[skip_bytes + 1] as u32) << 8;
                                    totloop += (buffer[skip_bytes + 2] as u32) << 16;
                                    totloop += (buffer[skip_bytes + 3] as u32) << 24;
                                    // println!("    totloop = {}", totloop);
                                    skip_bytes += 4;
                                    // mat_nr
                                    // let mut mat_nr: u16 = 0;
                                    // mat_nr += (buffer[skip_bytes] as u16) << 0;
                                    // mat_nr += (buffer[skip_bytes + 1] as u16) << 8;
                                    // println!("    mat_nr = {}", mat_nr);
                                    skip_bytes += 2;
                                    // flag
                                    let flag: u8 = buffer[skip_bytes];
                                    // println!("    flag = {}", flag);
                                    is_smooth = flag % 2 == 1;
                                    // println!("    is_smooth = {}", is_smooth);
                                    skip_bytes += 1;
                                    // pad
                                    // println!("    pad = {}", buffer[skip_bytes]);
                                    skip_bytes += 1;
                                    // PBRT
                                    if totloop == 3_u32 {
                                        loops.push(totloop as u8);
                                        // triangle
                                        for i in 0..3 {
                                            vertex_indices.push(
                                                loop_indices[(loopstart + i) as usize] as u32,
                                            );
                                        }
                                    } else if totloop == 4_u32 {
                                        loops.push(totloop as u8);
                                        // quads
                                        vertex_indices
                                            .push(loop_indices[(loopstart + 0) as usize] as u32);
                                        vertex_indices
                                            .push(loop_indices[(loopstart + 1) as usize] as u32);
                                        vertex_indices
                                            .push(loop_indices[(loopstart + 2) as usize] as u32);
                                        vertex_indices
                                            .push(loop_indices[(loopstart + 0) as usize] as u32);
                                        vertex_indices
                                            .push(loop_indices[(loopstart + 2) as usize] as u32);
                                        vertex_indices
                                            .push(loop_indices[(loopstart + 3) as usize] as u32);
                                    } else {
                                        println!(
                                            "WARNING: quads or triangles expected (totloop = {}): {:?}",
                                            totloop, base_name
                                        )
                                    }
                                }
                            // println!("  vertex_indices = {:?}", vertex_indices);
                            } else if types[type_id] == "MVert" {
                                // println!("{}[{}] ({})", code, data_len, len);
                                // println!("  SDNAnr = {}", sdna_nr);
                                // println!("  {} ({})", types[type_id], tlen[type_id]);
                                let mut skip_bytes: usize = 0;
                                let factor: f32 = 1.0 / 32767.0;
                                let mut coords: [f32; 3] = [0.0_f32; 3];
                                for _v in 0..data_len {
                                    // println!("  {}:", v + 1);
                                    // co
                                    for i in 0..3 {
                                        let mut co_buf: [u8; 4] = [0_u8; 4];
                                        for b in 0..4 as usize {
                                            co_buf[b] = buffer[skip_bytes + b];
                                        }
                                        let co: f32 = unsafe { mem::transmute(co_buf) };
                                        // println!("    co[{}] = {}", i, co);
                                        coords[i] = co;
                                        skip_bytes += 4;
                                    }
                                    coords[0] = coords[0] * scale_length;
                                    coords[1] = coords[1] * scale_length;
                                    coords[2] = coords[2] * scale_length;
                                    p.push(coords);
                                    // no
                                    for i in 0..3 {
                                        let mut no: i16 = 0;
                                        no += (buffer[skip_bytes] as i16) << 0;
                                        no += (buffer[skip_bytes + 1] as i16) << 8;
                                        let nof: f32 = no as f32 * factor;
                                        // println!("    no[{}] = {}", i, nof);
                                        coords[i] = nof;
                                        skip_bytes += 2;
                                    }
                                    n.push(coords);
                                    // flag
                                    // println!("    flag = {}", buffer[skip_bytes]);
                                    skip_bytes += 1;
                                    // bweight
                                    // println!("    bweight = {}", buffer[skip_bytes]);
                                    skip_bytes += 1;
                                }
                            // for v in 0..data_len as usize {
                            //     println!("  {}:", v + 1);
                            //     println!("    co: {:?}", p[v]);
                            //     println!("    no: {:?}", n[v]);
                            // }
                            } else if types[type_id] == "MLoop" {
                                // println!("{}[{}] ({})", code, data_len, len);
                                // println!("  SDNAnr = {}", sdna_nr);
                                // println!("  {} ({})", types[type_id], tlen[type_id]);
                                let mut skip_bytes: usize = 0;
                                for _l in 0..data_len {
                                    // println!("  {}:", l + 1);
                                    // v
                                    let mut v: u32 = 0;
                                    v += (buffer[skip_bytes] as u32) << 0;
                                    v += (buffer[skip_bytes + 1] as u32) << 8;
                                    v += (buffer[skip_bytes + 2] as u32) << 16;
                                    v += (buffer[skip_bytes + 3] as u32) << 24;
                                    // println!("    v = {}", v);
                                    loop_indices.push(v);
                                    skip_bytes += 4;
                                    // e
                                    // let mut e: u32 = 0;
                                    // e += (buffer[skip_bytes] as u32) << 0;
                                    // e += (buffer[skip_bytes + 1] as u32) << 8;
                                    // e += (buffer[skip_bytes + 2] as u32) << 16;
                                    // e += (buffer[skip_bytes + 3] as u32) << 24;
                                    // println!("    e = {}", e);
                                    skip_bytes += 4;
                                }
                            // println!("    loop_indices = {:?}", loop_indices);
                            } else if types[type_id] == "MLoopUV" {
                                // println!("{}[{}] ({})", code, data_len, len);
                                // println!("  SDNAnr = {}", sdna_nr);
                                // println!("  {} ({})", types[type_id], tlen[type_id]);
                                let mut skip_bytes: usize = 0;
                                let mut coords: [f32; 2] = [0.0_f32; 2];
                                for _l in 0..data_len {
                                    // println!("  {}:", l + 1);
                                    // float uv[2]
                                    for i in 0..2 {
                                        let mut uv_buf: [u8; 4] = [0_u8; 4];
                                        for b in 0..4 as usize {
                                            uv_buf[b] = buffer[skip_bytes + b];
                                        }
                                        let uv: f32 = unsafe { mem::transmute(uv_buf) };
                                        // println!("    uv[{}] = {}", i, uv);
                                        coords[i] = uv;
                                        skip_bytes += 4;
                                    }
                                    uvs.push(coords);
                                    // int flag
                                    skip_bytes += 4;
                                }
                            // for l in 0..data_len as usize {
                            //     println!("  {}:", l + 1);
                            //     println!("    uv: {:?}", uvs[l]);
                            // }
                            } else if types[type_id] == "MLoopCol" {
                                // println!("{}[{}] ({})", code, data_len, len);
                                // println!("  SDNAnr = {}", sdna_nr);
                                // println!("  {} ({})", types[type_id], tlen[type_id]);
                                // println!("  base_name = {}", base_name);
                                let mut skip_bytes: usize = 0;
                                for _l in 0..data_len {
                                    // r
                                    let mut red: u8 = 0;
                                    red += buffer[skip_bytes];
                                    skip_bytes += 1;
                                    // g
                                    let mut green: u8 = 0;
                                    green += buffer[skip_bytes];
                                    skip_bytes += 1;
                                    // b
                                    let mut blue: u8 = 0;
                                    blue += buffer[skip_bytes];
                                    skip_bytes += 1;
                                    // a
                                    let mut alpha: u8 = 0;
                                    alpha += buffer[skip_bytes];
                                    skip_bytes += 1;
                                    // println!("{}: {}, {}, {}, {}", l, red, green, blue, alpha);
                                    vertex_colors.push(red);
                                    vertex_colors.push(green);
                                    vertex_colors.push(blue);
                                    vertex_colors.push(alpha);
                                }
                                // println!("vertex_colors: {:?}", vertex_colors);
                            }
                        } else if data_following_object {
                            // type_id
                            let type_id: usize = dna_2_type_id[sdna_nr as usize] as usize;
                            if types[type_id] == "IDProperty" {
                                // println!("{}[{}] ({})", code, data_len, len);
                                // println!("  SDNAnr = {}", sdna_nr);
                                // println!("  {} ({})", types[type_id], tlen[type_id]);
                                let mut skip_bytes: usize = 0;
                                for _p in 0..data_len {
                                    // println!("  {}:", p + 1);
                                    // next
                                    skip_bytes += 8;
                                    // prev
                                    skip_bytes += 8;
                                    // type
                                    let prop_type: u8 = buffer[skip_bytes] as u8;
                                    // println!("  prop_type = {}", prop_type);
                                    skip_bytes += 1;
                                    if prop_type == 8_u8 {
                                        // IDP_DOUBLE (see DNA_ID.h)
                                        // subtype
                                        skip_bytes += 1;
                                        // flag
                                        skip_bytes += 2;
                                        // char name[64]
                                        let mut prop_name = String::new();
                                        for i in 0..64 {
                                            if buffer[skip_bytes + i] == 0 {
                                                break;
                                            }
                                            prop_name.push(buffer[skip_bytes + i] as char);
                                        }
                                        // if prop_name.len() > 0 {
                                        //     println!(
                                        //         "  prop_name[{}] = {}",
                                        //         prop_name.len(),
                                        //         prop_name
                                        //     );
                                        // }
                                        skip_bytes += 64;
                                        // saved
                                        skip_bytes += 4;
                                        // v279: data (len=32)
                                        // v280: data (len=32)
                                        skip_bytes += 8;
                                        // group
                                        skip_bytes += 16;
                                        // val
                                        // val2
                                        let mut val_buf: [u8; 8] = [0_u8; 8];
                                        for b in 0..8 as usize {
                                            val_buf[b] = buffer[skip_bytes + b];
                                        }
                                        let prop_val: f64 = unsafe { mem::transmute(val_buf) };
                                        // println!("  prop_val = {}", prop_val);
                                        skip_bytes += 8;
                                        if base_name.starts_with("PbrtSphere")
                                            || base_name.starts_with("PbrtCylinder")
                                            || base_name.starts_with("PbrtDisk")
                                        {
                                            // println!(
                                            //     "  {}.{} = {}",
                                            //     base_name, prop_name, prop_val
                                            // );
                                            // if prop_name == String::from("height") {
                                            //     prop_height = prop_val;
                                            // }
                                            if prop_name == String::from("radius") {
                                                prop_radius = prop_val;
                                            }
                                            // if prop_name == String::from("innerradius") {
                                            //     prop_innerradius = prop_val;
                                            // }
                                            // if prop_name == String::from("zmin") {
                                            //     prop_zmin = prop_val;
                                            // }
                                            // if prop_name == String::from("zmax") {
                                            //     prop_zmax = prop_val;
                                            // }
                                            // if prop_name == String::from("phimax") {
                                            //     prop_phimax = prop_val;
                                            // }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if data_following_object {
                            // TODO: PbrtCylinder, PbrtDisk
                            if base_name.starts_with("PbrtSphere") {
                                // store sphere values for later
                                let pbrt_sphere: PbrtSphere = PbrtSphere::new(prop_radius as f32);
                                spheres_hm.insert(base_name.clone(), pbrt_sphere);
                            }
                        }
                        if data_following_mesh {
                            // TODO: PbrtCylinder, PbrtDisk
                            if base_name.starts_with("PbrtSphere") {
                                // create sphere after mesh data
                                if let Some(sphere) = spheres_hm.get(&base_name) {
                                    builder.add_sphere(base_name.clone(), sphere.radius);
                                }
                            } else {
                                read_mesh(
                                    &base_name,
                                    &p,
                                    &n,
                                    &mut uvs,
                                    &loops,
                                    vertex_indices.clone(),
                                    vertex_colors.clone(),
                                    is_smooth,
                                    &mut builder,
                                );
                                vertex_indices.clear();
                                vertex_colors.clear();
                            }
                        }
                        // reset booleans
                        data_following_mesh = false;
                        data_following_object = false;
                        is_smooth = false;
                    }
                    if code != String::from("DATA")
                        && code != String::from("REND")
                        && code != String::from("TEST")
                    {
                        let type_id: usize = dna_2_type_id[sdna_nr as usize] as usize;
                        if len != tlen[type_id] as u32 {
                            println!("WARNING: {} ({} != {})", code, len, tlen[type_id]);
                        }
                    }
                }
            }
            println!("{} bytes read", counter);
        }
    }
    let scene_description: SceneDescription = builder.finalize();
    // write header of .ass file
    let mut of = File::create("btoa.ass")?;
    let mut of = BufWriter::new(of);
    {
        let dt: DateTime<Local> = Local::now();
        let exported = format!(
            "### exported: {}\n",
            dt.format("%a %b %e %T %Y").to_string()
        );
        of.write_all(exported.as_bytes())?;
        let clang = "clang-5.0.0";
        let oiio = "oiio-2.1.4";
        let osl = "osl-1.11.0";
        let vdb = "vdb-4.0.0";
        let clm = "clm-1.1.1.118";
        let rlm = "rlm-12.4.2";
        let optix = "optix-6.5.0";
        let date = "2019/12/04";
        let time = "07:45:07";
        let copied = format!(
            "linux {} {} {} {} {} {} {} {} {}",
            clang, oiio, osl, vdb, clm, rlm, optix, date, time
        );
        let arnold = format!(
            "### from:     {} {} [{}] {}\n",
            "Arnold", "6.0.1.0", "25372a4c", copied
        );
        of.write_all(arnold.as_bytes())?;
        let host_app = format!("### host app: ");
        of.write_all(host_app.as_bytes())?;
        Cli::clap().write_version(&mut of).unwrap();
        of.write_all(b"\n\n\n\n")?;
    }
    // println!("number of lights = {:?}", render_options.lights.len());
    // println!(
    //     "number of primitives = {:?}",
    //     render_options.primitives.len()
    // );
    base_name = String::from("Camera");
    if let Some(camera_name) = args.camera_name {
        base_name = camera_name.clone();
    }
    // options
    of.write_all(b"options\n{\n")?;
    let render_x: u32 = resolution_x * resolution_percentage as u32 / 100_u32;
    let render_y: u32 = resolution_y * resolution_percentage as u32 / 100_u32;
    let aspect: f32 = render_x as f32 / render_y as f32;
    println!(
        "{}x{} [{}%] = {}x{}",
        resolution_x, resolution_y, resolution_percentage, render_x, render_y
    );
    let xres = format!(" xres {}\n", render_x);
    of.write_all(xres.as_bytes())?;
    let yres = format!(" yres {}\n", render_y);
    of.write_all(yres.as_bytes())?;
    let camera = format!(" camera {:?}\n", base_name);
    of.write_all(camera.as_bytes())?;
    let gi_diffuse_depth = format!(" GI_diffuse_depth 1\n");
    of.write_all(gi_diffuse_depth.as_bytes())?;
    let gi_specular_depth = format!(" GI_specular_depth 1\n");
    of.write_all(gi_specular_depth.as_bytes())?;
    of.write_all(b"}\n\n")?;
    // camera(s)
    for key in camera_hm.keys() {
        if let Some(cam) = camera_hm.get(key) {
            if let Some(o2w) = object_to_world_hm.get(key) {
                // persp_camera
                of.write_all(b"persp_camera\n{\n")?;
                let name = format!(" name {}\n", key);
                of.write_all(name.as_bytes())?;
                of.write_all(b" matrix\n")?;
                let matrix = format!(
                    " {:.8e} {:.8e} {:.8e} {:.8e}\n",
                    o2w[0], o2w[1], o2w[2], o2w[3]
                );
                of.write_all(matrix.as_bytes())?;
                let matrix = format!(
                    " {:.8e} {:.8e} {:.8e} {:.8e}\n",
                    o2w[4], o2w[5], o2w[6], o2w[7]
                );
                of.write_all(matrix.as_bytes())?;
                let matrix = format!(
                    " {:.8e} {:.8e} {:.8e} {:.8e}\n",
                    o2w[8], o2w[9], o2w[10], o2w[11]
                );
                of.write_all(matrix.as_bytes())?;
                let matrix = format!(
                    " {:.8e} {:.8e} {:.8e} {:.8e}\n",
                    o2w[12], o2w[13], o2w[14], o2w[15]
                );
                of.write_all(matrix.as_bytes())?;
                let fov: f32;
                if aspect >= 1.0 {
                    fov = cam.angle_x;
                } else {
                    fov = 2.0 as f32 * degrees(((aspect * 16.0 as f32) / cam.lens).atan());
                }
                let fov = format!(" fov {}\n", fov);
                of.write_all(fov.as_bytes())?;
                of.write_all(b"}\n\n")?;
            }
        }
    }
    // light(s)
    for light_idx in 0..scene_description.light_names.len() {
        let light_name = &scene_description.light_names[light_idx];
        let light = &scene_description.lights[light_idx];
        if let Some(o2w) = object_to_world_hm.get(light_name) {
            match light {
                AssLight::AssDistant(_lgt) => {
                    of.write_all(b"distant_light\n{\n")?;
                }
                AssLight::AssPoint(_lgt) => {
                    of.write_all(b"point_light\n{\n")?;
                }
            }
            let name = format!(" name {}\n", light_name);
            of.write_all(name.as_bytes())?;
            of.write_all(b" matrix\n")?;
            let matrix = format!(
                " {:.8e} {:.8e} {:.8e} {:.8e}\n",
                o2w[0], o2w[1], o2w[2], o2w[3]
            );
            of.write_all(matrix.as_bytes())?;
            let matrix = format!(
                " {:.8e} {:.8e} {:.8e} {:.8e}\n",
                o2w[4], o2w[5], o2w[6], o2w[7]
            );
            of.write_all(matrix.as_bytes())?;
            let matrix = format!(
                " {:.8e} {:.8e} {:.8e} {:.8e}\n",
                o2w[8], o2w[9], o2w[10], o2w[11]
            );
            of.write_all(matrix.as_bytes())?;
            let matrix = format!(
                " {:.8e} {:.8e} {:.8e} {:.8e}\n",
                o2w[12], o2w[13], o2w[14], o2w[15]
            );
            of.write_all(matrix.as_bytes())?;
            match light {
                AssLight::AssDistant(lgt) => {
                    let color = format!(
                        " color {:.8e} {:.8e} {:.8e}\n",
                        lgt.color[0], lgt.color[1], lgt.color[2]
                    );
                    of.write_all(color.as_bytes())?;
                    let intensity = format!(" intensity {:.8e}\n", lgt.intensity);
                    of.write_all(intensity.as_bytes())?;
                    of.write_all(b"}\n\n")?;
                }
                AssLight::AssPoint(lgt) => {
                    let color = format!(
                        " color {:.8e} {:.8e} {:.8e}\n",
                        lgt.color[0], lgt.color[1], lgt.color[2]
                    );
                    of.write_all(color.as_bytes())?;
                    let intensity = format!(" intensity {:.8e}\n", lgt.intensity);
                    of.write_all(intensity.as_bytes())?;
                    of.write_all(b"}\n\n")?;
                }
            }
        }
    }
    // sphere(s)
    for sphere_idx in 0..scene_description.sphere_names.len() {
        let sphere_name = &scene_description.sphere_names[sphere_idx];
        let sphere = &scene_description.spheres[sphere_idx];
        let mut opaque: bool = true;
        if let Some(mat) = get_material(sphere_name, &material_hm) {
            // standard_surface
            of.write_all(b"standard_surface\n{\n")?;
            let name = format!(" name {}\n", sphere_name);
            of.write_all(name.as_bytes())?;
            if mat.emit > 0.0 {
                let base_color = format!(" base_color {} {} {}\n", mat.r, mat.g, mat.b);
                of.write_all(base_color.as_bytes())?;
                let diffuse_roughness = format!(" diffuse_roughness {}\n", 0.0);
                of.write_all(diffuse_roughness.as_bytes())?;
                let specular = format!(" specular 0\n");
                of.write_all(specular.as_bytes())?;
                let specular_roughness = format!(" specular_roughness {}\n", 0.0);
                of.write_all(specular_roughness.as_bytes())?;
                let emission = format!(" emission {}\n", mat.emit * args.light_scale);
                of.write_all(emission.as_bytes())?;
                let emission_color = format!(" emission_color {} {} {}\n", mat.r, mat.g, mat.b);
                of.write_all(emission_color.as_bytes())?;
            } else {
                if mat.ang != 1.0 {
                    opaque = false;
                    // glass
                    let base = format!(" base 0\n");
                    of.write_all(base.as_bytes())?;
                    let specular = format!(" specular 1\n");
                    of.write_all(specular.as_bytes())?;
                    let specular_color = format!(" specular_color 1 1 1\n");
                    of.write_all(specular_color.as_bytes())?;
                    let specular_roughness = format!(" specular_roughness {}\n", mat.roughness);
                    of.write_all(specular_roughness.as_bytes())?;
                    let specular_ior = format!(" specular_IOR {}\n", mat.ang);
                    of.write_all(specular_ior.as_bytes())?;
                    let transmission = format!(" transmission 1\n");
                    of.write_all(transmission.as_bytes())?;
                    let transmission_color =
                        format!(" transmission_color {} {} {}\n", mat.r, mat.g, mat.b);
                    of.write_all(transmission_color.as_bytes())?;
                } else if mat.ray_mirror > 0.0 {
                    let base_color = format!(" base_color {} {} {}\n", mat.r, mat.g, mat.b);
                    of.write_all(base_color.as_bytes())?;
                    let diffuse_roughness = format!(" diffuse_roughness {}\n", 0.0);
                    of.write_all(diffuse_roughness.as_bytes())?;
                    let specular = format!(" specular 0\n");
                    of.write_all(specular.as_bytes())?;
                    let specular_roughness = format!(" specular_roughness {}\n", 0.0);
                    of.write_all(specular_roughness.as_bytes())?;
                    let metalness = format!(" metalness {}\n", mat.ray_mirror);
                    of.write_all(metalness.as_bytes())?;
                    if mat.roughness > 0.0 {
                        // metal
                    } else {
                        // mirror
                    }
                } else {
                    // matte
                    let base_color = format!(" base_color {} {} {}\n", mat.r, mat.g, mat.b);
                    of.write_all(base_color.as_bytes())?;
                    let diffuse_roughness = format!(" diffuse_roughness {}\n", 0.0);
                    of.write_all(diffuse_roughness.as_bytes())?;
                    let specular = format!(" specular 0\n");
                    of.write_all(specular.as_bytes())?;
                    let specular_roughness = format!(" specular_roughness {}\n", 0.0);
                    of.write_all(specular_roughness.as_bytes())?;
                }
            }
            of.write_all(b"}\n\n")?;
        }
        if let Some(o2w) = object_to_world_hm.get(sphere_name) {
            of.write_all(b"sphere\n{\n")?;
            let name = format!(" name {}\n", sphere_name);
            of.write_all(name.as_bytes())?;
            of.write_all(b" matrix\n")?;
            let matrix = format!(
                " {:.8e} {:.8e} {:.8e} {:.8e}\n",
                o2w[0], o2w[1], o2w[2], o2w[3]
            );
            of.write_all(matrix.as_bytes())?;
            let matrix = format!(
                " {:.8e} {:.8e} {:.8e} {:.8e}\n",
                o2w[4], o2w[5], o2w[6], o2w[7]
            );
            of.write_all(matrix.as_bytes())?;
            let matrix = format!(
                " {:.8e} {:.8e} {:.8e} {:.8e}\n",
                o2w[8], o2w[9], o2w[10], o2w[11]
            );
            of.write_all(matrix.as_bytes())?;
            let matrix = format!(
                " {:.8e} {:.8e} {:.8e} {:.8e}\n",
                o2w[12], o2w[13], o2w[14], o2w[15]
            );
            of.write_all(matrix.as_bytes())?;
            let shader = format!(" shader {:?}\n", sphere_name);
            of.write_all(shader.as_bytes())?;
            if opaque {
                let opaque = format!(" opaque true\n");
                of.write_all(opaque.as_bytes())?;
            } else {
                let opaque = format!(" opaque false\n");
                of.write_all(opaque.as_bytes())?;
            }
            let radius = format!(" radius {:?}\n", sphere.radius);
            of.write_all(radius.as_bytes())?;
            of.write_all(b"}\n\n")?;
        }
    }
    // polymesh(es)
    for mesh_idx in 0..scene_description.mesh_names.len() {
        let mesh_name = &scene_description.mesh_names[mesh_idx];
        let mesh = &scene_description.meshes[mesh_idx];
        let shader_colors = &scene_description.mesh_shader_colors[mesh_idx];
        let mut diff_tex: bool = false;
        if let Some(tex) = texture_hm.get(mesh_name) {
            // first try texture with exactly the same name as the mesh
            of.write_all(b"image\n{\n")?;
            let name = format!(" name {}_diff_tex\n", mesh_name);
            of.write_all(name.as_bytes())?;
            let filename = format!("filename {:?}\n", tex.to_str().unwrap());
            of.write_all(filename.as_bytes())?;
            of.write_all(b"}\n\n")?;
            diff_tex = true;
        } else {
            // then remove trailing digits from mesh name
            let mut ntd: String = String::new();
            let mut chars = mesh_name.chars();
            let mut digits: String = String::new(); // many digits
            while let Some(c) = chars.next() {
                if c.is_digit(10_u32) {
                    // collect digits
                    digits.push(c);
                } else {
                    // push collected digits (if any)
                    ntd += &digits;
                    // and reset
                    digits = String::new();
                    // push non-digit
                    ntd.push(c);
                }
            }
            // try no trailing digits (ntd)
            if let Some(tex) = texture_hm.get(&ntd) {
                of.write_all(b"image\n{\n")?;
                let name = format!(" name {}_diff_tex\n", mesh_name);
                of.write_all(name.as_bytes())?;
                let filename = format!("filename {:?}\n", tex.to_str().unwrap());
                of.write_all(filename.as_bytes())?;
                of.write_all(b"}\n\n")?;
                diff_tex = true;
            }
        }
        let mut opaque: bool = false;
        if let Some(mat) = get_material(mesh_name, &material_hm) {
            // standard_surface
            if shader_colors.len() == 0_usize {
                of.write_all(b"standard_surface\n{\n")?;
                let name = format!(" name {}\n", mesh_name);
                of.write_all(name.as_bytes())?;
                if mat.emit > 0.0 {
                    let base_color = format!(" base_color {} {} {}\n", mat.r, mat.g, mat.b);
                    of.write_all(base_color.as_bytes())?;
                    let diffuse_roughness = format!(" diffuse_roughness {}\n", 0.0);
                    of.write_all(diffuse_roughness.as_bytes())?;
                    let specular = format!(" specular 0\n");
                    of.write_all(specular.as_bytes())?;
                    let specular_roughness = format!(" specular_roughness {}\n", 0.0);
                    of.write_all(specular_roughness.as_bytes())?;
                    let emission = format!(" emission {}\n", mat.emit * args.light_scale);
                    of.write_all(emission.as_bytes())?;
                    let emission_color = format!(" emission_color {} {} {}\n", mat.r, mat.g, mat.b);
                    of.write_all(emission_color.as_bytes())?;
                } else {
                    if mat.ang != 1.0 {
                        opaque = true;
                        // glass
                        let base = format!(" base 0\n");
                        of.write_all(base.as_bytes())?;
                        let specular = format!(" specular 1\n");
                        of.write_all(specular.as_bytes())?;
                        let specular_color = format!(" specular_color 1 1 1\n");
                        of.write_all(specular_color.as_bytes())?;
                        let specular_roughness = format!(" specular_roughness {}\n", mat.roughness);
                        of.write_all(specular_roughness.as_bytes())?;
                        let specular_ior = format!(" specular_IOR {}\n", mat.ang);
                        of.write_all(specular_ior.as_bytes())?;
                        let transmission = format!(" transmission 1\n");
                        of.write_all(transmission.as_bytes())?;
                        let transmission_color =
                            format!(" transmission_color {} {} {}\n", mat.r, mat.g, mat.b);
                        of.write_all(transmission_color.as_bytes())?;
                    } else if mat.ray_mirror > 0.0 {
                        let base_color = format!(" base_color {} {} {}\n", mat.r, mat.g, mat.b);
                        of.write_all(base_color.as_bytes())?;
                        let diffuse_roughness = format!(" diffuse_roughness {}\n", 0.0);
                        of.write_all(diffuse_roughness.as_bytes())?;
                        let specular = format!(" specular 0\n");
                        of.write_all(specular.as_bytes())?;
                        let specular_roughness = format!(" specular_roughness {}\n", 0.0);
                        of.write_all(specular_roughness.as_bytes())?;
                        let metalness = format!(" metalness {}\n", mat.ray_mirror);
                        of.write_all(metalness.as_bytes())?;
                        if mat.roughness > 0.0 {
                            // metal
                        } else {
                            // mirror
                        }
                    } else {
                        // matte
                        if diff_tex {
                            let base_color = format!(" base_color {}_diff_tex\n", mesh_name);
                            of.write_all(base_color.as_bytes())?;
                        } else {
                            let base_color = format!(" base_color {} {} {}\n", mat.r, mat.g, mat.b);
                            of.write_all(base_color.as_bytes())?;
                        }
                        let diffuse_roughness = format!(" diffuse_roughness {}\n", 0.0);
                        of.write_all(diffuse_roughness.as_bytes())?;
                        let specular = format!(" specular 0\n");
                        of.write_all(specular.as_bytes())?;
                        let specular_roughness = format!(" specular_roughness {}\n", 0.0);
                        of.write_all(specular_roughness.as_bytes())?;
                    }
                }
                of.write_all(b"}\n\n")?;
            } else {
                for shader_color_idx in 0..shader_colors.len() {
                    let shader_color = shader_colors[shader_color_idx];
                    of.write_all(b"standard_surface\n{\n")?;
                    let name = format!(" name {}_{}\n", mesh_name, shader_color_idx);
                    of.write_all(name.as_bytes())?;
                    // matte
                    let base_color = format!(
                        " base_color {} {} {}\n",
                        shader_color[0], shader_color[1], shader_color[2]
                    );
                    of.write_all(base_color.as_bytes())?;
                    let diffuse_roughness = format!(" diffuse_roughness {}\n", 0.0);
                    of.write_all(diffuse_roughness.as_bytes())?;
                    let specular = format!(" specular 0\n");
                    of.write_all(specular.as_bytes())?;
                    let specular_roughness = format!(" specular_roughness {}\n", 0.0);
                    of.write_all(specular_roughness.as_bytes())?;
                    of.write_all(b"}\n\n")?;
                }
            }
        }
        if let Some(o2w) = object_to_world_hm.get(mesh_name) {
            // polymesh
            of.write_all(b"polymesh\n{\n")?;
            let name = format!(" name {}\n", mesh_name);
            of.write_all(name.as_bytes())?;
            of.write_all(b" matrix\n")?;
            let matrix = format!(
                " {:.8e} {:.8e} {:.8e} {:.8e}\n",
                o2w[0], o2w[1], o2w[2], o2w[3]
            );
            of.write_all(matrix.as_bytes())?;
            let matrix = format!(
                " {:.8e} {:.8e} {:.8e} {:.8e}\n",
                o2w[4], o2w[5], o2w[6], o2w[7]
            );
            of.write_all(matrix.as_bytes())?;
            let matrix = format!(
                " {:.8e} {:.8e} {:.8e} {:.8e}\n",
                o2w[8], o2w[9], o2w[10], o2w[11]
            );
            of.write_all(matrix.as_bytes())?;
            let matrix = format!(
                " {:.8e} {:.8e} {:.8e} {:.8e}\n",
                o2w[12], o2w[13], o2w[14], o2w[15]
            );
            of.write_all(matrix.as_bytes())?;
            if shader_colors.len() == 0_usize {
                // one shaders
                let shader = format!(" shader {:?}\n", mesh_name);
                of.write_all(shader.as_bytes())?;
                if opaque {
                    let opaque = format!(" opaque true\n");
                    of.write_all(opaque.as_bytes())?;
                } else {
                    let opaque = format!(" opaque false\n");
                    of.write_all(opaque.as_bytes())?;
                }
            } else {
                // several shaders
                let shader = format!(" shader {} 1 node\n", shader_colors.len());
                of.write_all(shader.as_bytes())?;
                of.write_all(b"  ")?;
                for shader_color_idx in 0..shader_colors.len() {
                    let name = format!("\"{}_{}\" ", mesh_name, shader_color_idx);
                    of.write_all(name.as_bytes())?;
                }
                of.write_all(b"\n")?;
                let opaque = format!(" opaque true\n");
                of.write_all(opaque.as_bytes())?;
            }
            let nsides = format!(" nsides {} 1 UINT\n", mesh.vidxs.len() / 3);
            of.write_all(nsides.as_bytes())?;
            of.write_all(b"  ")?;
            for _tri in 0..(mesh.vidxs.len() / 3) {
                let value = format!("{} ", 3);
                of.write_all(value.as_bytes())?;
            }
            of.write_all(b"\n")?;
            if shader_colors.len() > 0_usize {
                let nsides = format!(" shidxs {} 1 BYTE\n", mesh.shidxs.len());
                of.write_all(nsides.as_bytes())?;
                of.write_all(b"  ")?;
                for shidx in 0..mesh.shidxs.len() {
                    let value = format!("{} ", mesh.shidxs[shidx]);
                    of.write_all(value.as_bytes())?;
                }
                of.write_all(b"\n")?;
            }
            let vidxs = format!(" vidxs {} 1 UINT\n", mesh.vidxs.len());
            of.write_all(vidxs.as_bytes())?;
            of.write_all(b"  ")?;
            for vidx in &mesh.vidxs {
                let value = format!("{} ", vidx);
                of.write_all(value.as_bytes())?;
            }
            of.write_all(b"\n")?;
            let vlist = format!(" vlist {} 1 VECTOR\n", mesh.vlist.len());
            of.write_all(vlist.as_bytes())?;
            for v in &mesh.vlist {
                let value = format!("  {} {} {}\n", v[0], v[1], v[2]);
                of.write_all(value.as_bytes())?;
            }
            if !mesh.nlist.is_empty() && mesh.vlist.len() == mesh.nlist.len() {
                let nidxs = format!(" nidxs {} 1 UINT\n", mesh.vidxs.len());
                of.write_all(nidxs.as_bytes())?;
                of.write_all(b"  ")?;
                for vidx in &mesh.vidxs {
                    let value = format!("{} ", vidx);
                    of.write_all(value.as_bytes())?;
                }
                of.write_all(b"\n")?;
                let nlist = format!(" nlist {} 1 VECTOR\n", mesh.nlist.len());
                of.write_all(nlist.as_bytes())?;
                for n in &mesh.nlist {
                    let value = format!("  {} {} {}\n", n[0], n[1], n[2]);
                    of.write_all(value.as_bytes())?;
                }
                let smoothing = format!(" smoothing true\n");
                of.write_all(smoothing.as_bytes())?;
            }
            if diff_tex && mesh.uvlist.len() > 0 {
                let uvidxs = format!(" uvidxs {} 1 UINT\n", mesh.vidxs.len());
                of.write_all(uvidxs.as_bytes())?;
                of.write_all(b"  ")?;
                for uvidx in &mesh.vidxs {
                    let value = format!("{} ", uvidx);
                    of.write_all(value.as_bytes())?;
                }
                of.write_all(b"\n")?;
                let uvlist = format!(" uvlist {} 1 VECTOR2\n", mesh.uvlist.len());
                of.write_all(uvlist.as_bytes())?;
                for uv in &mesh.uvlist {
                    let value = format!("  {} {}\n", uv[0], uv[1]);
                    of.write_all(value.as_bytes())?;
                }
            }
            of.write_all(b"}\n\n")?;
        }
    }
    Ok(())
}
