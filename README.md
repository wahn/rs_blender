<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#orgc18f236">1. blend_info</a></li>
<li><a href="#org881e463">2. btoa</a></li>
</ul>
</div>
</div>
Tools written in [Rust](https://www.rust-lang.org) to deal with [Blender](https://www.blender.org) files.


<a id="orgc18f236"></a>

# blend\_info

    > cd blend_info/
    > cargo build --release
    ...
    > ./target/release/blend_info --help
    blend_info 0.1.0
    Jan Walter <jan@janwalter.com>
    Print some information about a Blender scene file.
    
    USAGE:
        blend_info <path>
    
    FLAGS:
        -h, --help       Prints help information
        -V, --version    Prints version information
    
    ARGS:
        <path>    The path to the file to read


<a id="org881e463"></a>

# btoa

    > cd btoa/
    > cargo build --release
    ...
    > ./target/release/btoa --help
    btoa 0.1.0
    Parse a Blender scene file and export it in Arnold's .ass file format.
    
    USAGE:
        btoa [OPTIONS] <path>
    
    FLAGS:
        -h, --help       Prints help information
        -V, --version    Prints version information
    
    OPTIONS:
        -c, --camera_name <camera-name>            camera name
        -l, --light_scale <light-scale>            global light scaling [default: 1.0]
        -o, --output_filename <output-filename>    output filename (default: "btoa.ass")
    
    ARGS:
        <path>    The path to the file to read

