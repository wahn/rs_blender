all: clean

clean:
	-rm -f *~ README.html* src/*~
	cd blend_info ; make clean ; cd ..
	cd btoa ; make clean ; cd ..

clobber: clean
	cd blend_info ; make clobber ; cd ..
	cd btoa ; make clobber ; cd ..
