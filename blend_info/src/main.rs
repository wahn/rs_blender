use std::fs::File;
use std::io::Read;
use std::mem;
use std::string::String;
use structopt::StructOpt;

/// Print some information about a Blender scene file.
#[derive(StructOpt)]
struct Cli {
    /// The path to the file to read
    #[structopt(parse(from_os_str))]
    path: std::path::PathBuf,
}

fn decode_blender_header(header: &[u8], version: &mut u32) -> bool {
    // BLENDER
    match header[0] as char {
        'B' => print!("B"),
        _ => return false,
    }
    match header[1] as char {
        'L' => print!("L"),
        _ => return false,
    }
    match header[2] as char {
        'E' => print!("E"),
        _ => return false,
    }
    match header[3] as char {
        'N' => print!("N"),
        _ => return false,
    }
    match header[4] as char {
        'D' => print!("D"),
        _ => return false,
    }
    match header[5] as char {
        'E' => print!("E"),
        _ => return false,
    }
    match header[6] as char {
        'R' => print!("R"),
        _ => return false,
    }
    // [_|-]
    match header[7] as char {
        '_' => print!("_"),
        '-' => print!("-"),
        _ => return false,
    }
    // [v|V]
    match header[8] as char {
        'v' => print!("v"),
        'V' => print!("V"),
        _ => return false,
    }
    for i in 9..12 {
        if header[i].is_ascii_digit() {
            print!("{:?}", (header[i] as char).to_digit(10).unwrap());
        } else {
            return false;
        }
    }
    print!("\n");
    // get the version number
    let last3c = vec!(header[9], header[10], header[11]);
    let version_str = String::from_utf8(last3c).unwrap(); // last 3 chars
    // convert to u32 and return
    *version = version_str.parse::<u32>().unwrap();
    true
}

fn make_id(code: &[u8]) -> String {
    let mut id = String::with_capacity(4);
    for i in 0..4 {
        if (code[i] as char).is_ascii_alphanumeric() {
            id.push(code[i] as char);
        }
    }
    id
}

fn do_print(code: &String) -> bool {
    if *code != String::from("BR")
        && *code != String::from("DATA")
        && *code != String::from("DNA1")
        && *code != String::from("GLOB")
        && *code != String::from("GR")
        && *code != String::from("LS")
        && *code != String::from("REND")
        && *code != String::from("SC")
        && *code != String::from("SN")
        && *code != String::from("TEST")
        && *code != String::from("WM")
        && *code != String::from("WS")
    {
        true
    } else {
        false
    }
}

fn get_id_name(buffer: &[u8]) -> String {
    // DNA_ID.h:240
    let mut id_name = String::new();
    // skip 4 pointers (next, prev, newid, lib)
    //println!("{:?}", &buffer[0..32]);
    // char name[66];
    for i in 32..98 {
        let byte: char = buffer[i] as char;
        if buffer[i] == 0 {
            break;
        } else {
            id_name.push(byte as char);
        }
    }
    id_name
}

fn read_names(
    f: &mut File,
    nr_names: usize,
    names: &mut Vec<String>,
    byte_counter: &mut usize,
) -> std::io::Result<()> {
    let mut name_counter: usize = 0;
    let mut buffer = [0; 1];
    loop {
        if names.len() == nr_names {
            break;
        } else {
            let mut name = String::new();
            loop {
                // read only one char/byte
                f.read(&mut buffer)?;
                *byte_counter += 1;
                if buffer[0] == 0 {
                    break;
                } else {
                    name.push(buffer[0] as char);
                }
            }
            // println!("  {:?}", name);
            names.push(name);
            name_counter += 1;
        }
    }
    println!("  {} names found in {} bytes", name_counter, byte_counter);
    Ok(())
}

fn read_type_names(
    f: &mut File,
    nr_types: usize,
    type_names: &mut Vec<String>,
    byte_counter: &mut usize,
) -> std::io::Result<()> {
    let mut name_counter: usize = 0;
    let mut buffer = [0; 1];
    loop {
        if type_names.len() == nr_types {
            break;
        } else {
            let mut name = String::new();
            loop {
                // read only one char/byte
                f.read(&mut buffer)?;
                *byte_counter += 1;
                if buffer[0] == 0 {
                    break;
                } else {
                    name.push(buffer[0] as char);
                }
            }
            // println!("  {:?}", name);
            type_names.push(name);
            name_counter += 1;
        }
    }
    println!(
        "  {} type names found in {} bytes",
        name_counter, byte_counter
    );
    Ok(())
}

fn main() -> std::io::Result<()> {
    let args = Cli::from_args();
    let mut f = File::open(&args.path)?;
    // read exactly 12 bytes
    let mut counter: usize = 0;
    let mut buffer = [0; 12];
    f.read(&mut buffer)?;
    counter += 12;
    let mut blender_version: u32 = 0;
    if !decode_blender_header(&buffer, &mut blender_version) {
        println!("ERROR: Not a .blend file");
        println!("First 12 bytes:");
        println!("{:?}", buffer);
    } else {
        let mut current_code: String = String::new();
        let mut current_name: String = String::new();
        let mut data_counter: u32 = 0;
        let mut is_camera: bool = false;
        let mut is_light: bool = false;
        let mut is_material: bool = false;
        let mut is_mesh: bool = false;
        let mut is_object: bool = false;
        loop {
            // check 4 chars
            let mut buffer = [0; 4];
            f.read(&mut buffer)?;
            counter += 4;
            let code = make_id(&buffer);
            let mut buffer = [0; 4];
            f.read(&mut buffer)?;
            counter += 4;
            let mut len: u32 = 0;
            len += (buffer[0] as u32) << 0;
            len += (buffer[1] as u32) << 8;
            len += (buffer[2] as u32) << 16;
            len += (buffer[3] as u32) << 24;
            if code != String::from("DATA") {
                if current_code != String::from("")
                // && data_counter != 0
                {
                    if do_print(&current_code) {
                        println!("  {:?} has {} data blocks", current_name, data_counter);
                        // reset
                        is_camera = false;
                        is_light = false;
                        is_material = false;
                        is_mesh = false;
                        is_object = false;
                    }
                }
                current_code = code.clone();
            } else {
                data_counter += 1;
            }
            if do_print(&code) {
                println!("{} ({})", code, len);
                if current_code == String::from("CA") {
                    is_camera = true;
                } else {
                    is_camera = false;
                }
                if current_code == String::from("LA") {
                    is_light = true;
                } else {
                    is_light = false;
                }
                if current_code == String::from("MA") {
                    is_material = true;
                } else {
                    is_material = false;
                }
                if current_code == String::from("ME") {
                    is_mesh = true;
                } else {
                    is_mesh = false;
                }
                if current_code == String::from("OB") {
                    is_object = true;
                } else {
                    is_object = false;
                }
            }
            if code != String::from("DATA") {
                // reset
                data_counter = 0;
            }
            // for now ignore the old entry
            let mut buffer = [0; 8];
            f.read(&mut buffer)?;
            counter += 8;
            // get SDNAnr
            let mut buffer = [0; 4];
            f.read(&mut buffer)?;
            counter += 4;
            let mut sdna_nr: u32 = 0;
            sdna_nr += (buffer[0] as u32) << 0;
            sdna_nr += (buffer[1] as u32) << 8;
            sdna_nr += (buffer[2] as u32) << 16;
            sdna_nr += (buffer[3] as u32) << 24;
            // for now ignore the nr entry
            let mut buffer = [0; 4];
            f.read(&mut buffer)?;
            counter += 4;
            if code == String::from("DATA") {
                if is_camera || is_light || is_material || is_mesh || is_object {
                    // use last four bytes to see how many entries there are
                    let mut data_len: u32 = 0;
                    data_len += (buffer[0] as u32) << 0;
                    data_len += (buffer[1] as u32) << 8;
                    data_len += (buffer[2] as u32) << 16;
                    data_len += (buffer[3] as u32) << 24;
                    println!("  DATA[{}] (SDNAnr = {})", data_len, sdna_nr);
                }
            }
            // are we done?
            if code == String::from("ENDB") {
                break;
            }
            if code == String::from("DNA1") {
                println!("{} ({})", code, len);
                // "SDNANAME" in first 8 bytes
                let mut buffer = [0; 8];
                f.read(&mut buffer)?;
                counter += 8;
                let mut sdna_name = String::with_capacity(8);
                for i in 0..8 {
                    if (buffer[i] as char).is_ascii_alphabetic() {
                        sdna_name.push(buffer[i] as char);
                    }
                }
                if sdna_name != String::from("SDNANAME") {
                    println!("WARNING: \"SDNANAME\" expected, {:?} found", sdna_name);
                    // read remaining bytes
                    let mut buffer = vec![0; (len - 8) as usize];
                    f.read(&mut buffer)?;
                    counter += (len - 8) as usize;
                } else {
                    println!("  {}", sdna_name);
                    let mut buffer = [0; 4];
                    f.read(&mut buffer)?;
                    counter += 4;
                    let mut nr_names: u32 = 0;
                    nr_names += (buffer[0] as u32) << 0;
                    nr_names += (buffer[1] as u32) << 8;
                    nr_names += (buffer[2] as u32) << 16;
                    nr_names += (buffer[3] as u32) << 24;
                    println!("  expect {} names", nr_names);
                    let mut names: Vec<String> = Vec::with_capacity(nr_names as usize);
                    let mut names_len: usize = 0;
                    read_names(&mut f, nr_names as usize, &mut names, &mut names_len)?;
                    counter += names_len;
                    let mut remaining_bytes: usize = (len - 12) as usize - names_len;
                    // skip pad bytes, read "TYPE" and nr_types
                    let mut buffer = [0; 1];
                    loop {
                        f.read(&mut buffer)?;
                        counter += 1;
                        if buffer[0] == 0 {
                            // skip pad byte
                            remaining_bytes -= 1;
                        } else if buffer[0] as char == 'T' {
                            remaining_bytes -= 1;
                            break;
                        }
                    }
                    // match 'YPE' ('T' was matched above)
                    let mut buffer = [0; 3];
                    f.read(&mut buffer)?;
                    counter += 3;
                    remaining_bytes -= 3;
                    if buffer[0] as char == 'Y'
                        && buffer[1] as char == 'P'
                        && buffer[2] as char == 'E'
                    {
                        // nr_types
                        let mut buffer = [0; 4];
                        f.read(&mut buffer)?;
                        counter += 4;
                        remaining_bytes -= 4;
                        let mut nr_types: u32 = 0;
                        nr_types += (buffer[0] as u32) << 0;
                        nr_types += (buffer[1] as u32) << 8;
                        nr_types += (buffer[2] as u32) << 16;
                        nr_types += (buffer[3] as u32) << 24;
                        println!("  expect {} type names", nr_types);
                        let mut types: Vec<String> = Vec::with_capacity(nr_types as usize);
                        let mut types_len: usize = 0;
                        read_type_names(&mut f, nr_types as usize, &mut types, &mut types_len)?;
                        counter += types_len;
                        remaining_bytes -= types_len;
                        // store tlen (type len) here
                        let mut tlen: Vec<u16> = Vec::new();
                        // skip pad bytes, read "TLEN"
                        let mut buffer = [0; 1];
                        loop {
                            f.read(&mut buffer)?;
                            counter += 1;
                            if buffer[0] == 0 {
                                // skip pad byte
                                remaining_bytes -= 1;
                            } else if buffer[0] as char == 'T' {
                                remaining_bytes -= 1;
                                break;
                            }
                        }
                        // match 'LEN' ('T' was matched above)
                        let mut buffer = [0; 3];
                        f.read(&mut buffer)?;
                        counter += 3;
                        remaining_bytes -= 3;
                        if buffer[0] as char == 'L'
                            && buffer[1] as char == 'E'
                            && buffer[2] as char == 'N'
                        {
                            // read short (16 bits = 2 bytes) for each type
                            for _i in 0..nr_types as usize {
                                let mut buffer = [0; 2];
                                f.read(&mut buffer)?;
                                counter += 2;
                                remaining_bytes -= 2;
                                let mut type_size: u16 = 0;
                                type_size += (buffer[0] as u16) << 0;
                                type_size += (buffer[1] as u16) << 8;
                                // println!("  {} needs {} bytes", types[i], type_size);
                                tlen.push(type_size);
                            }
                            // skip pad bytes, read "STRC"
                            let mut buffer = [0; 1];
                            loop {
                                f.read(&mut buffer)?;
                                counter += 1;
                                if buffer[0] == 0 {
                                    // skip pad byte
                                    remaining_bytes -= 1;
                                } else if buffer[0] as char == 'S' {
                                    remaining_bytes -= 1;
                                    break;
                                }
                            }
                            // match 'TRC' ('S' was matched above)
                            let mut buffer = [0; 3];
                            f.read(&mut buffer)?;
                            counter += 3;
                            remaining_bytes -= 3;
                            if buffer[0] as char == 'T'
                                && buffer[1] as char == 'R'
                                && buffer[2] as char == 'C'
                            {
                                // nr_structs
                                let mut buffer = [0; 4];
                                f.read(&mut buffer)?;
                                counter += 4;
                                remaining_bytes -= 4;
                                let mut nr_structs: u32 = 0;
                                nr_structs += (buffer[0] as u32) << 0;
                                nr_structs += (buffer[1] as u32) << 8;
                                nr_structs += (buffer[2] as u32) << 16;
                                nr_structs += (buffer[3] as u32) << 24;
                                println!("  expect {} struct pointers", nr_structs);
                                for s in 0..nr_structs as usize {
                                    // read two short values
                                    let mut buffer = [0; 2];
                                    f.read(&mut buffer)?;
                                    counter += 2;
                                    remaining_bytes -= 2;
                                    let mut type_idx: u16 = 0;
                                    type_idx += (buffer[0] as u16) << 0;
                                    type_idx += (buffer[1] as u16) << 8;
                                    f.read(&mut buffer)?;
                                    counter += 2;
                                    remaining_bytes -= 2;
                                    let mut short2: u16 = 0;
                                    short2 += (buffer[0] as u16) << 0;
                                    short2 += (buffer[1] as u16) << 8;
                                    // println!("  ({}, {})", type_idx, short2);
                                    println!("  [SDNAnr = {}]", s);
                                    println!(
                                        "  {} (len={}) {{",
                                        types[type_idx as usize], tlen[type_idx as usize]
                                    );
                                    let tuple_counter: usize = short2 as usize;
                                    for _t in 0..tuple_counter {
                                        // read two short values
                                        let mut buffer = [0; 2];
                                        f.read(&mut buffer)?;
                                        counter += 2;
                                        remaining_bytes -= 2;
                                        let mut type_idx: u16 = 0;
                                        type_idx += (buffer[0] as u16) << 0;
                                        type_idx += (buffer[1] as u16) << 8;
                                        f.read(&mut buffer)?;
                                        counter += 2;
                                        remaining_bytes -= 2;
                                        let mut name_idx: u16 = 0;
                                        name_idx += (buffer[0] as u16) << 0;
                                        name_idx += (buffer[1] as u16) << 8;
                                        println!(
                                            "    {} {};",
                                            types[type_idx as usize], names[name_idx as usize]
                                        );
                                    }
                                    println!("  }}");
                                }
                            } else {
                                println!("ERROR: \"STRC\" expected, \"S\"{:?} found", buffer)
                            }
                        } else {
                            println!("ERROR: \"TLEN\" expected, \"T\"{:?} found", buffer)
                        }
                    } else {
                        println!("ERROR: \"TYPE\" expected, \"T\"{:?} found", buffer)
                    }
                    // read remaining bytes
                    println!("  remaining bytes: {}", remaining_bytes);
                    let mut buffer = vec![0; remaining_bytes];
                    f.read(&mut buffer)?;
                    counter += remaining_bytes;
                }
            } else {
                // read len bytes
                let mut buffer = vec![0; len as usize];
                f.read(&mut buffer)?;
                counter += len as usize;
                if do_print(&code) {
                    current_name = get_id_name(&buffer);
                }
                if code == String::from("CA") {
                    // see struct Camera (DNA_camera_types.h)
                    // skip ID bytes (len=152 or 120)
                    let mut id_len: usize = 152;
                    if blender_version < 280 {
                        id_len = 120;
                    }
                    // skip AnimData *adt (len=8)
                    // char type
                    let ca_type: u8 = buffer[id_len + 8];
                    match ca_type {
                        0 => println!("  type = CAM_PERSP"),
                        1 => println!("  type = CAM_ORTHO"),
                        2 => println!("  type = CAM_PANO"),
                        _ => println!("WARNING: unknown camera type = {}", ca_type),
                    }
                    // skip dtx
                    // skip flag
                    // skip passepartalpha
                    // clipsta
                    let mut clipsta_buf: [u8; 4] = [0_u8; 4];
                    for i in 0..4 as usize {
                        clipsta_buf[i] = buffer[id_len+16+i];
                    }
                    let clipsta: f32 = unsafe { mem::transmute(clipsta_buf) };
                    println!("  clipsta = {}", clipsta);
                    // clipend
                    let mut clipend_buf: [u8; 4] = [0_u8; 4];
                    for i in 0..4 as usize {
                        clipend_buf[i] = buffer[id_len+20+i];
                    }
                    let clipend: f32 = unsafe { mem::transmute(clipend_buf) };
                    println!("  clipend = {}", clipend);
                    // lens
                    let mut lens_buf: [u8; 4] = [0_u8; 4];
                    for i in 0..4 as usize {
                        lens_buf[i] = buffer[id_len+24+i];
                    }
                    let lens: f32 = unsafe { mem::transmute(lens_buf) };
                    println!("  lens = {}", lens);
                    // ortho_scale
                    let mut ortho_scale_buf: [u8; 4] = [0_u8; 4];
                    for i in 0..4 as usize {
                        ortho_scale_buf[i] = buffer[id_len+28+i];
                    }
                    let ortho_scale: f32 = unsafe { mem::transmute(ortho_scale_buf) };
                    println!("  ortho_scale = {}", ortho_scale);
                    // drawsize
                    let mut drawsize_buf: [u8; 4] = [0_u8; 4];
                    for i in 0..4 as usize {
                        drawsize_buf[i] = buffer[id_len+32+i];
                    }
                    let drawsize: f32 = unsafe { mem::transmute(drawsize_buf) };
                    println!("  drawsize = {}", drawsize);
                }
            }
        }
        println!("{} bytes read", counter);
    }
    Ok(())
}
