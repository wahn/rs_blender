    ./blend_info --help
    blend_info 0.1.0
    Print some information about a Blender scene file.
    
    USAGE:
        blend_info <path>
    
    FLAGS:
        -h, --help       Prints help information
        -V, --version    Prints version information
    
    ARGS:
        <path>    The path to the file to read

